package com.shortcircuit.bazaar.selection;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import java.util.Optional;
import java.util.UUID;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleOptions;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.util.Color;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/9/2018.
 */
@ConfigSerializable
public class ShopSelection {
	private UUID world;
	@Setting("min-pos")
	private Vector3i min_pos;
	@Setting("max-pos")
	private Vector3i max_pos;
	
	public ShopSelection() {
	
	}
	
	public void setWorld(World world) {
		this.world = world.getUniqueId();
	}
	
	public Optional<World> getWorld() {
		return Sponge.getServer().getWorld(world);
	}
	
	public void setMinPos(Vector3i min_pos) {
		this.min_pos = min_pos;
	}
	
	public Vector3i getMinPos() {
		return min_pos;
	}
	
	public void setMaxPos(Vector3i max_pos) {
		this.max_pos = max_pos;
	}
	
	public Vector3i getMaxPos() {
		return max_pos;
	}
	
	public Pair<Vector3i, Vector3i> getNormalizedPoints() {
		if (min_pos == null || max_pos == null) {
			return null;
		}
		return Pair.of(
				new Vector3i(
						Math.min(min_pos.getX(), max_pos.getX()),
						Math.min(min_pos.getY(), max_pos.getY()),
						Math.min(min_pos.getZ(), max_pos.getZ())
				),
				new Vector3i(
						Math.max(min_pos.getX(), max_pos.getX()),
						Math.max(min_pos.getY(), max_pos.getY()),
						Math.max(min_pos.getZ(), max_pos.getZ())
				)
		);
	}
	
	public final void showPoints() {
		Optional<World> world_opt = getWorld();
		if (!world_opt.isPresent()) {
			return;
		}
		World world = world_opt.get();
		if (min_pos != null) {
			ParticleEffect effect = ParticleEffect.builder()
					.type(ParticleTypes.REDSTONE_DUST)
					.offset(new Vector3d(0.6, 0.6, 0.6))
					.option(ParticleOptions.COLOR, Color.LIME)
					.quantity(64)
					.velocity(Vector3d.ZERO)
					.build();
			world.spawnParticles(effect, min_pos.toDouble().add(0.5, 0.5, 0.5));
		}
		if (max_pos != null) {
			ParticleEffect effect = ParticleEffect.builder()
					.type(ParticleTypes.REDSTONE_DUST)
					.offset(new Vector3d(0.6, 0.6, 0.6))
					.option(ParticleOptions.COLOR, Color.BLUE)
					.quantity(64)
					.velocity(Vector3d.ZERO)
					.build();
			world.spawnParticles(effect, max_pos.toDouble().add(0.5, 0.5, 0.5));
		}
	}
	
	public final void showEdges() {
		showEdges(0.125);
	}
	
	public final void showEdges(double step) {
		Optional<World> world_opt = getWorld();
		if (!world_opt.isPresent()) {
			return;
		}
		World world = world_opt.get();
		ParticleEffect effect;
		Pair<Vector3i, Vector3i> normalized = getNormalizedPoints();
		if (normalized == null) {
			return;
		}
		Vector3d min_pos = normalized.getLeft().toDouble();
		Vector3d max_pos = normalized.getRight().toDouble();
		effect = ParticleEffect.builder()
				.type(ParticleTypes.END_ROD)
				.build();
		/*
		// X
		for (double x = min_pos.getX(); x <= max_pos.getX(); x += 0.125) {
			world.spawnParticles(effect, new Vector3d(x + 0.5, min_pos.getY() + 0.5, min_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(x + 0.5, min_pos.getY() + 0.5, max_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(x + 0.5, max_pos.getY() + 0.5, min_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(x + 0.5, max_pos.getY() + 0.5, max_pos.getZ() + 0.5));
		}
		
		// Y
		for (double y = min_pos.getY(); y <= max_pos.getY(); y += 0.125) {
			world.spawnParticles(effect, new Vector3d(min_pos.getX() + 0.5, y + 0.5, min_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(min_pos.getX() + 0.5, y + 0.5, max_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 0.5, y + 0.5, min_pos.getZ() + 0.5));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 0.5, y + 0.5, max_pos.getZ() + 0.5));
		}
		// Z
		for (double z = min_pos.getZ(); z <= max_pos.getZ(); z += 0.125) {
			world.spawnParticles(effect, new Vector3d(min_pos.getX() + 0.5, min_pos.getY() + 0.5, z + 0.5));
			world.spawnParticles(effect, new Vector3d(min_pos.getX() + 0.5, max_pos.getY() + 0.5, z + 0.5));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 0.5, min_pos.getY() + 0.5, z + 0.5));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 0.5, max_pos.getY() + 0.5, z + 0.5));
		}
		*/
		final double vertical_offset = 0.0625;
		// X
		for (double x = min_pos.getX(); x <= max_pos.getX() + 1; x += step) {
			world.spawnParticles(effect, new Vector3d(x, min_pos.getY() + vertical_offset, min_pos.getZ()));
			world.spawnParticles(effect, new Vector3d(x, min_pos.getY() + vertical_offset, max_pos.getZ() + 1));
			world.spawnParticles(effect, new Vector3d(x, max_pos.getY() + vertical_offset + 1, min_pos.getZ()));
			world.spawnParticles(effect, new Vector3d(x, max_pos.getY() + vertical_offset + 1, max_pos.getZ() + 1));
		}
		
		// Y
		for (double y = min_pos.getY(); y <= max_pos.getY() + 1; y += step) {
			world.spawnParticles(effect, new Vector3d(min_pos.getX(), y + vertical_offset, min_pos.getZ()));
			world.spawnParticles(effect, new Vector3d(min_pos.getX(), y + vertical_offset, max_pos.getZ() + 1));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 1, y + vertical_offset, min_pos.getZ()));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 1, y + vertical_offset, max_pos.getZ() + 1));
		}
		// Z
		for (double z = min_pos.getZ(); z <= max_pos.getZ() + 1; z += step) {
			world.spawnParticles(effect, new Vector3d(min_pos.getX(), min_pos.getY() + vertical_offset, z));
			world.spawnParticles(effect, new Vector3d(min_pos.getX(), max_pos.getY() + 1 + vertical_offset, z));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 1, min_pos.getY() + vertical_offset, z));
			world.spawnParticles(effect, new Vector3d(max_pos.getX() + 1, max_pos.getY() + vertical_offset + 1, z));
		}
	}
	
	public void onStartSelection(Player player) {
	
	}
	
	public void onCancelSelection(Player player) {
	
	}
	
	public boolean onStopSelection(Player player) {
		return true;
	}
	
	public String getSelectorName() {
		return "selection";
	}
}
