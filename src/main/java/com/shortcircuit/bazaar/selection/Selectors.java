package com.shortcircuit.bazaar.selection;

import com.shortcircuit.bazaar.Bazaar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.event.message.MessageChannelEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.ItemTypes;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/9/2018.
 */
public class Selectors {
	private static final HashMap<UUID, ShopSelection> selectors = new HashMap<>();
	
	public static class EdgeVisualizer implements Runnable {
		@Override
		public void run() {
			for (ShopSelection selection : new HashSet<>(selectors.values())) {
				selection.showEdges();
				selection.showPoints();
			}
		}
		
		@Listener
		public void setMinPos(final InteractBlockEvent.Primary event, @Root Player player) {
			Optional<Location<World>> location_opt = event.getTargetBlock().getLocation();
			ShopSelection selection = selectors.get(player.getUniqueId());
			Optional<ItemStack> hand = player.getItemInHand(HandTypes.MAIN_HAND);
			if (hand.isPresent() && selection != null && location_opt.isPresent()) {
				Optional<ItemType> type_opt = Sponge.getRegistry().getType(ItemType.class, Bazaar.getInstance().getConfig().selection_wand_type);
				ItemType type = type_opt.orElse(ItemTypes.GOLDEN_PICKAXE);
				if (hand.get().getItem().getType().equals(type)) {
					selection.setWorld(location_opt.get().getExtent());
					selection.setMinPos(location_opt.get().getBlockPosition());
					event.setCancelled(true);
				}
			}
		}
		
		@Listener
		public void setMaxPos(final InteractBlockEvent.Secondary event, @Root Player player) {
			Optional<Location<World>> location_opt = event.getTargetBlock().getLocation();
			ShopSelection selection = selectors.get(player.getUniqueId());
			Optional<ItemStack> hand = player.getItemInHand(HandTypes.MAIN_HAND);
			if (hand.isPresent() && selection != null && location_opt.isPresent()) {
				Optional<ItemType> type_opt = Sponge.getRegistry().getType(ItemType.class, Bazaar.getInstance().getConfig().selection_wand_type);
				ItemType type = type_opt.orElse(ItemTypes.GOLDEN_PICKAXE);
				if (hand.get().getItem().getType().equals(type)) {
					selection.setWorld(location_opt.get().getExtent());
					selection.setMaxPos(location_opt.get().getBlockPosition());
					event.setCancelled(true);
				}
			}
		}
		
		@Listener
		public void listenToChat(final MessageChannelEvent.Chat event, @Root Player player) {
			if (selectors.containsKey(player.getUniqueId())) {
				ShopSelection selector;
				switch (event.getRawMessage().toPlain().toLowerCase()) {
					case "confirm":
						selector = selectors.remove(player.getUniqueId());
						if (selector.onStopSelection(player)) {
							player.sendMessages(Text.of(TextColors.AQUA, "Created ", selector.getSelectorName()));
						}
						else {
							selectors.put(player.getUniqueId(), selector);
						}
						break;
					case "cancel":
						selector = selectors.remove(player.getUniqueId());
						player.sendMessages(Text.of(TextColors.RED, "Stopped selecting"));
						selector.onCancelSelection(player);
						break;
					default:
						selector = selectors.get(player.getUniqueId());
						player.sendMessages(
								Text.of(TextColors.AQUA, "Type ", TextColors.GREEN, "confirm", TextColors.AQUA, " in chat to create the ", selector.getSelectorName()),
								Text.of(TextColors.AQUA, "Type ", TextColors.RED, "cancel", TextColors.AQUA, " in chat to stop selecting")
						);
						break;
				}
				event.setCancelled(true);
			}
		}
	}
	
	public static boolean initSelector(Player player, ShopSelection selector) {
		Optional<ItemType> type_opt = Sponge.getRegistry().getType(ItemType.class, Bazaar.getInstance().getConfig().selection_wand_type);
		ItemType type = type_opt.orElse(ItemTypes.GOLDEN_PICKAXE);
		ItemStack wand = ItemStack.builder()
				.itemType(type)
				.quantity(1)
				.build();
		if (player.getInventory().query(type).size() > 0) {
			player.sendMessages(Text.of(TextColors.AQUA, "Use your ", TextColors.GOLD, wand.getTranslation(), TextColors.AQUA, " to select the points"));
		}
		else {
			if (player.getItemInHand(HandTypes.MAIN_HAND).isPresent() && !player.getInventory().offer(wand).getRejectedItems().isEmpty()) {
				player.sendMessages(Text.of(TextColors.RED, "You have no empty slot for the wand"));
				return false;
			}
			else {
				player.setItemInHand(HandTypes.MAIN_HAND, wand);
			}
		}
		player.sendMessages(
				Text.of(TextColors.AQUA, "Left-click to set the ", TextColors.GREEN, "first point"),
				Text.of(TextColors.AQUA, "Right-click to set the ", TextColors.BLUE, "second point"),
				Text.of(TextColors.AQUA, "Type ", TextColors.GREEN, "confirm", TextColors.AQUA, " in chat to create the stall"),
				Text.of(TextColors.AQUA, "Type ", TextColors.RED, "cancel", TextColors.AQUA, " in chat to stop selecting")
		);
		selector.setWorld(player.getWorld());
		selector.onStartSelection(player);
		selectors.put(player.getUniqueId(), selector);
		return true;
	}
}
