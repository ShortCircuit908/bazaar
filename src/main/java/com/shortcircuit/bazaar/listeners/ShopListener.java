package com.shortcircuit.bazaar.listeners;

import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.event.DoubleClickBlockEvent;
import com.shortcircuit.bazaar.market.ShopSign;
import com.shortcircuit.bazaar.util.DumbCompatibilityStuff;
import com.shortcircuit.bazaar.util.EconomyUtils;
import com.shortcircuit.bazaar.util.ItemUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.service.economy.account.Account;
import org.spongepowered.api.service.economy.transaction.ResultType;
import org.spongepowered.api.service.economy.transaction.TransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/11/2018.
 */
public class ShopListener {
	@Listener
	public void validateShopCreation(final ChangeSignEvent event, @Root Player player) {
		if (event.isCancelled()) {
			return;
		}
		Sponge.getScheduler().createTaskBuilder()
				.delayTicks(1)
				.execute(new Runnable() {
					@Override
					public void run() {
						if (!event.isCancelled()) {
							if (new ShopSign(event.getTargetTile(), player).isValid()) {
								player.sendMessages(Text.of(TextColors.AQUA, "Right-click while sneaking to set item"));
							}
						}
					}
				})
				.submit(Bazaar.getInstance());
	}
	
	@Listener
	public void createShop(final InteractBlockEvent.Secondary.MainHand event, @Root Player player) {
		BlockSnapshot snapshot = event.getTargetBlock();
		if (snapshot.getLocation().isPresent() && player.get(Keys.IS_SNEAKING).orElse(false)) {
			Location<World> location = snapshot.getLocation().get();
			Optional<TileEntity> tile_entity_opt = location.getTileEntity();
			if (tile_entity_opt.isPresent()) {
				TileEntity tile_entity = tile_entity_opt.get();
				if (tile_entity.supports(Keys.SIGN_LINES)) {
					ShopSign shop_sign = new ShopSign(tile_entity);
					if (shop_sign.isValid()
							&& ((shop_sign.isAdminShop() && player.hasPermission(ShopSign.PERMISSION_CREATE_ADMIN))
							|| (shop_sign.getOwner().isPresent() && shop_sign.getOwner().get().getUniqueId().equals(player.getUniqueId())))) {
						Optional<ItemStack> item_opt = player.getItemInHand(HandTypes.MAIN_HAND);
						if (item_opt.isPresent()) {
							shop_sign.setItem(item_opt.get().createSnapshot());
							event.setCancelled(true);
						}
					}
				}
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void buyItem(final InteractBlockEvent.Secondary event, @Root Player player) {
		BlockSnapshot snapshot = event.getTargetBlock();
		if (snapshot.getLocation().isPresent() && !player.get(Keys.IS_SNEAKING).orElse(false)) {
			Location<World> location = snapshot.getLocation().get();
			Optional<TileEntity> tile_entity_opt = location.getTileEntity();
			if (tile_entity_opt.isPresent()) {
				TileEntity tile_entity = tile_entity_opt.get();
				if (tile_entity.supports(Keys.SIGN_LINES)) {
					ShopSign shop_sign = new ShopSign(tile_entity);
					if (shop_sign.isFullyConfigured() && (shop_sign.isAdminShop() || !shop_sign.getOwner().get().getUniqueId().equals(player.getUniqueId()))) {
						if (!shop_sign.getSellPrice().isPresent()) {
							player.sendMessages(Text.of(TextColors.RED, "This shop is not selling"));
							return;
						}
						double total = shop_sign.getSellPrice().get();
						Account source_account = EconomyUtils.getUserAccount(player.getUniqueId());
						Account target_account = shop_sign.isAdminShop() ?
								(Bazaar.getInstance().getConfig().use_server_money ? EconomyUtils.getServerAccount() : null)
								: EconomyUtils.getUserAccount(shop_sign.getOwner().get().getUniqueId());
						
						int quantity = shop_sign.getQuantity();
						ItemStack item = shop_sign.getItem().get().createStack();
						int max_stack_size = item.getMaxStackQuantity();
						List<ItemStack> items = new ArrayList<>(quantity / max_stack_size + 1);
						while (quantity > 0) {
							int to_add = Math.min(quantity, max_stack_size);
							ItemStack clone = item.copy();
							clone.setQuantity(to_add);
							items.add(clone);
							quantity -= to_add;
						}
						Inventory source = null;
						Optional<Carrier> carrier_opt = shop_sign.findAdjascentChest();
						if (carrier_opt.isPresent()) {
							source = carrier_opt.get().getInventory();
						}
						if (source == null && !shop_sign.isAdminShop()) {
							player.sendMessages(Text.of(TextColors.RED, "This shop is missing a chest"));
							return;
						}
						int has_quantity = ItemUtils.getTotalQuantity(item, source);
						if (has_quantity < shop_sign.getQuantity()) {
							player.sendMessages(Text.of(TextColors.RED, "This shop is out of stock"));
							Optional<User> owner = shop_sign.getOwner();
							if (owner.isPresent() && owner.get().getPlayer().isPresent()) {
								owner.get().getPlayer().get().sendMessages(Text.of(TextColors.RED, "Your ", TextColors.GOLD,
										item.getTranslation(), TextColors.RED, " shop is out of stock"));
							}
							return;
						}
						Inventory dest = player.getInventory();
						boolean does_fit = ItemUtils.doesItemFit(items, dest);
						if (!does_fit) {
							player.sendMessage(Text.of(TextColors.RED, "You do not have enough space in your inventory"));
							return;
						}
						TransactionResult transaction_result = source_account.withdraw(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(total), DumbCompatibilityStuff.source(Bazaar.getInstance()));
						if (transaction_result.getResult().equals(ResultType.ACCOUNT_NO_FUNDS)) {
							player.sendMessages(Text.of(TextColors.RED, "Insufficient funds"));
							return;
						}
						if (!transaction_result.getResult().equals(ResultType.SUCCESS)) {
							player.sendMessages(Text.of(TextColors.RED, "A problem occurred with the transaction"));
							return;
						}
						ItemUtils.transfer(items, source, dest, shop_sign.isAdminShop());
						player.sendMessages(Text.of(TextColors.AQUA, "You purchased ", TextColors.GOLD, shop_sign.getQuantity(),
								TextColors.AQUA, " x ", TextColors.GOLD, item.getTranslation(), TextColors.AQUA, " for ",
								TextColors.GOLD, EconomyUtils.getDefaultCurrency().getSymbol(), total));
						if (target_account != null) {
							target_account.deposit(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(total), DumbCompatibilityStuff.source(Bazaar.getInstance()));
							Optional<User> owner = shop_sign.getOwner();
							if (owner.isPresent() && owner.get().getPlayer().isPresent()) {
								owner.get().getPlayer().get().sendMessages(Text.of(TextColors.GOLD,
										player.getDisplayNameData().displayName().get(), TextColors.AQUA, " purchased ",
										TextColors.GOLD, shop_sign.getQuantity(), TextColors.AQUA, " x ", TextColors.GOLD,
										item.getTranslation(), TextColors.AQUA, " for ", TextColors.GOLD,
										EconomyUtils.getDefaultCurrency().getSymbol(), total));
							}
						}
					}
					if (shop_sign.isValid()) {
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void buyItem(final InteractBlockEvent.Primary event, @Root Player player) {
		BlockSnapshot snapshot = event.getTargetBlock();
		if (snapshot.getLocation().isPresent() && !player.get(Keys.IS_SNEAKING).orElse(false)) {
			Location<World> location = snapshot.getLocation().get();
			Optional<TileEntity> tile_entity_opt = location.getTileEntity();
			if (tile_entity_opt.isPresent()) {
				TileEntity tile_entity = tile_entity_opt.get();
				if (tile_entity.supports(Keys.SIGN_LINES)) {
					ShopSign shop_sign = new ShopSign(tile_entity);
					if (shop_sign.isFullyConfigured() && (shop_sign.isAdminShop() || !shop_sign.getOwner().get().getUniqueId().equals(player.getUniqueId()))) {
						if (!shop_sign.getBuyPrice().isPresent()) {
							player.sendMessages(Text.of(TextColors.RED, "This shop is not buying"));
							return;
						}
						double total = shop_sign.getBuyPrice().get();
						Account target_account = EconomyUtils.getUserAccount(player.getUniqueId());
						Account source_account = shop_sign.isAdminShop() ?
								(Bazaar.getInstance().getConfig().use_server_money ? EconomyUtils.getServerAccount() : null)
								: EconomyUtils.getUserAccount(shop_sign.getOwner().get().getUniqueId());
						
						int quantity = shop_sign.getQuantity();
						ItemStack item = shop_sign.getItem().get().createStack();
						int max_stack_size = item.getMaxStackQuantity();
						List<ItemStack> items = new ArrayList<>(quantity / max_stack_size + 1);
						while (quantity > 0) {
							int to_add = Math.min(quantity, max_stack_size);
							ItemStack clone = item.copy();
							clone.setQuantity(to_add);
							items.add(clone);
							quantity -= to_add;
						}
						Inventory dest = null;
						Optional<Carrier> carrier_opt = shop_sign.findAdjascentChest();
						if (carrier_opt.isPresent()) {
							dest = carrier_opt.get().getInventory();
						}
						if (dest == null && !shop_sign.isAdminShop()) {
							player.sendMessages(Text.of(TextColors.RED, "This shop is missing a chest"));
							return;
						}
						Inventory source = player.getInventory();
						
						int has_quantity = ItemUtils.getTotalQuantity(item, source);
						if (has_quantity < shop_sign.getQuantity()) {
							player.sendMessages(Text.of(TextColors.RED, "You do not have enough to sell"));
							return;
						}
						boolean does_fit = ItemUtils.doesItemFit(items, dest);
						if (!does_fit) {
							player.sendMessage(Text.of(TextColors.RED, "This shop is full"));
							return;
						}
						if (source_account != null) {
							TransactionResult transaction_result = source_account.withdraw(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(total), DumbCompatibilityStuff.source(Bazaar.getInstance()));
							if (transaction_result.getResult().equals(ResultType.ACCOUNT_NO_FUNDS)) {
								player.sendMessages(Text.of(TextColors.RED, "The shop owner has insufficient funds"));
								return;
							}
							if (!transaction_result.getResult().equals(ResultType.SUCCESS)) {
								player.sendMessages(Text.of(TextColors.RED, "A problem occurred with the transaction"));
								return;
							}
						}
						ItemUtils.transfer(items, source, dest, shop_sign.isAdminShop());
						player.sendMessages(Text.of(TextColors.AQUA, "You sold ", TextColors.GOLD, shop_sign.getQuantity(),
								TextColors.AQUA, " x ", TextColors.GOLD, item.getTranslation(), TextColors.AQUA, " for ",
								TextColors.GOLD, EconomyUtils.getDefaultCurrency().getSymbol(), total));
						target_account.deposit(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(total), DumbCompatibilityStuff.source(Bazaar.getInstance()));
						Optional<User> owner = shop_sign.getOwner();
						if (owner.isPresent() && owner.get().getPlayer().isPresent()) {
							owner.get().getPlayer().get().sendMessages(Text.of(TextColors.GOLD,
									player.getDisplayNameData().displayName().get(), TextColors.AQUA, " sold ",
									TextColors.GOLD, shop_sign.getQuantity(), TextColors.AQUA, " x ", TextColors.GOLD,
									item.getTranslation(), TextColors.AQUA, " for ", TextColors.GOLD,
									EconomyUtils.getDefaultCurrency().getSymbol(), total));
						}
					}
					if (shop_sign.isValid()) {
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	@Listener(order = Order.LAST)
	public void buyAllItems(final DoubleClickBlockEvent.Secondary event, @Root Player player) {
		if (event.isCancelled()) {
			return;
		}
		
	}
	
	@Listener(order = Order.LAST)
	public void sellAllItems(final DoubleClickBlockEvent.Primary event, @Root Player player) {
		if (event.isCancelled()) {
			return;
		}
	}
}
