package com.shortcircuit.bazaar.listeners;

import com.flowpowered.math.vector.Vector3i;
import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.market.Market;
import com.shortcircuit.bazaar.market.MarketStall;
import com.shortcircuit.bazaar.market.ShopSign;
import com.shortcircuit.bazaar.util.VectorUtils;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.Transaction;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.Entity;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.action.InteractEvent;
import org.spongepowered.api.event.block.ChangeBlockEvent;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.block.tileentity.ChangeSignEvent;
import org.spongepowered.api.event.entity.InteractEntityEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

/**
 * @author ShortCircuit908
 * Created on 1/15/2018.
 */
public class MarketListener {
	public static final HashMap<UUID, MarketStall> stall_sign_selectors = new HashMap<>();
	
	@Listener
	public void noInteract(final InteractEvent event, @Root Player player) {
		Optional<Vector3i> interaction_point_opt = Optional.empty();
		if (event instanceof InteractBlockEvent) {
			BlockSnapshot block = ((InteractBlockEvent) event).getTargetBlock();
			if (block.getLocation().isPresent()) {
				interaction_point_opt = Optional.of(block.getLocation().get().getBlockPosition());
			}
		}
		else if (event instanceof InteractEntityEvent) {
			Entity entity = ((InteractEntityEvent) event).getTargetEntity();
			interaction_point_opt = Optional.of(entity.getLocation().getBlockPosition());
		}
		if (!interaction_point_opt.isPresent()) {
			return;
		}
		Vector3i interaction_point = interaction_point_opt.get().toInt();
		Optional<Market> market_opt = Optional.empty();
		Optional<MarketStall> stall_opt = Optional.empty();
		for (Market market : Market.getMarkets()) {
			if (!market.getWorld().isPresent()) {
				continue;
			}
			if (market.getWorld().get().getUniqueId().equals(player.getWorld().getUniqueId())) {
				Pair<Vector3i, Vector3i> market_points = market.getNormalizedPoints();
				if (VectorUtils.isPointInsideVolume(interaction_point, market_points.getLeft(), market_points.getRight())) {
					market_opt = Optional.of(market);
					stall_opt = MarketStall.getStall(new Location<>(market.getWorld().get(), interaction_point));
					break;
				}
			}
		}
		Optional<TileEntity> tile_entity_opt = player.getWorld().getTileEntity(interaction_point);
		if (tile_entity_opt.isPresent() && tile_entity_opt.get().supports(Keys.SIGN_LINES)) {
			ShopSign shop_sign = new ShopSign(tile_entity_opt.get());
			if (shop_sign.isValid()) {
				event.setCancelled(false);
				return;
			}
			if (market_opt.isPresent()) {
				for (MarketStall stall : market_opt.get().getStalls()) {
					if (stall.getInfoSignPosition().isPresent() && stall.getInfoSignPosition().get().equals(interaction_point)) {
						event.setCancelled(false);
						return;
					}
				}
			}
		}
		if (market_opt.isPresent()) {
			if (player.hasPermission(Bazaar.PLUGIN_IDENTIFIER + ".ignoreprotections")) {
				event.setCancelled(false);
				return;
			}
			if (stall_opt.isPresent()) {
				MarketStall stall = stall_opt.get();
				Optional<User> owner_opt = stall.getOwner();
				if (owner_opt.isPresent()) {
					User owner = owner_opt.get();
					if (owner.getUniqueId().equals(player.getUniqueId())) {
						event.setCancelled(false);
						return;
					}
				}
			}
			for (MarketStall stall : market_opt.get().getStalls()) {
				if (stall.getOwner().isPresent() && stall.getOwner().get().getUniqueId().equals(player.getUniqueId())) {
					stall.showEdges();
				}
			}
			event.setCancelled(true);
		}
	}
	
	@Listener
	public void noInteract(final ChangeBlockEvent event, @Root Player player) {
		for (Transaction<BlockSnapshot> transaction : event.getTransactions()) {
			Vector3i interaction_point = transaction.getFinal().getPosition();
			Optional<Market> market_opt = Optional.empty();
			Optional<MarketStall> stall_opt = Optional.empty();
			for (Market market : Market.getMarkets()) {
				if (!market.getWorld().isPresent()) {
					continue;
				}
				if (market.getWorld().get().getUniqueId().equals(player.getWorld().getUniqueId())) {
					Pair<Vector3i, Vector3i> market_points = market.getNormalizedPoints();
					if (VectorUtils.isPointInsideVolume(interaction_point, market_points.getLeft(), market_points.getRight())) {
						market_opt = Optional.of(market);
						stall_opt = MarketStall.getStall(new Location<>(market.getWorld().get(), interaction_point));
						break;
					}
				}
			}
			Optional<TileEntity> tile_entity_opt = player.getWorld().getTileEntity(interaction_point);
			if (tile_entity_opt.isPresent() && tile_entity_opt.get().supports(Keys.SIGN_LINES)) {
				ShopSign shop_sign = new ShopSign(tile_entity_opt.get());
				if (shop_sign.isValid()) {
					event.setCancelled(false);
					return;
				}
				if (market_opt.isPresent()) {
					for (MarketStall stall : market_opt.get().getStalls()) {
						if (stall.getInfoSignPosition().isPresent() && stall.getInfoSignPosition().get().equals(interaction_point)) {
							event.setCancelled(false);
							return;
						}
					}
				}
			}
			if (market_opt.isPresent()) {
				if (player.hasPermission(Bazaar.PLUGIN_IDENTIFIER + ".ignoreprotections")) {
					event.setCancelled(false);
					return;
				}
				if (stall_opt.isPresent()) {
					MarketStall stall = stall_opt.get();
					Optional<User> owner_opt = stall.getOwner();
					if (owner_opt.isPresent()) {
						User owner = owner_opt.get();
						if (owner.getUniqueId().equals(player.getUniqueId())) {
							event.setCancelled(false);
							return;
						}
					}
				}
				for (MarketStall stall : market_opt.get().getStalls()) {
					if (stall.getOwner().isPresent() && stall.getOwner().get().getUniqueId().equals(player.getUniqueId())) {
						stall.showEdges();
					}
				}
				event.setCancelled(true);
			}
		}
	}
	
	@Listener
	public void noQuark(final ChangeSignEvent event) {
		Sign sign = event.getTargetTile();
		ShopSign shop_sign = new ShopSign(sign);
		if (shop_sign.isValid()) {
			event.setCancelled(true);
			return;
		}
		for (Market market : Market.getMarkets()) {
			for (MarketStall stall : market.getStalls()) {
				Optional<TileEntity> info_sign_opt = stall.getInfoSign();
				if (info_sign_opt.isPresent() && info_sign_opt.get().getLocation().equals(sign.getLocation())) {
					event.setCancelled(true);
					return;
				}
			}
		}
	}
	
	@Listener
	public void clickToRent(final InteractBlockEvent.Secondary event, @Root Player player) {
		if (!event.getInteractionPoint().isPresent()) {
			return;
		}
		BlockSnapshot snapshot = event.getTargetBlock();
		if (!snapshot.getLocation().isPresent()) {
			return;
		}
		Optional<TileEntity> entity_opt = snapshot.getLocation().get().getTileEntity();
		if (stall_sign_selectors.containsKey(player.getUniqueId())) {
			MarketStall stall = stall_sign_selectors.remove(player.getUniqueId());
			if (!entity_opt.isPresent() || !entity_opt.get().supports(Keys.SIGN_LINES)) {
				player.sendMessage(Text.of(TextColors.RED, "Cancelled info sign selection"));
				return;
			}
			stall.setInfoSign(entity_opt.get());
			player.sendMessage(Text.of(TextColors.AQUA, "Set the info sign for the stall"));
			return;
		}
		/*
		if (!entity_opt.isPresent() || !entity_opt.get().supports(Keys.SIGN_LINES) || !player.hasPermission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")) {
			return;
		}
		for (Market market : Market.getMarkets()) {
			for (MarketStall stall : market.getStalls()) {
				Optional<Vector3i> info_sign = stall.getInfoSignPosition();
				if (info_sign.isPresent()) {
					Vector3i info_sign_position = info_sign.get();
					if (!info_sign_position.equals(interaction_point) && !stall.getWorld().get().getUniqueId().equals(player.getWorld().getUniqueId())) {
						continue;
					}
					Optional<User> owner_opt = stall.getOwner();
					if (owner_opt.isPresent() && !owner_opt.get().getUniqueId().equals(player.getUniqueId())) {
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
						String date = stall.getEvictionDate().isPresent() ? format.format(stall.getEvictionDate().get()) : "basically forever";
						player.sendMessage(Text.of(TextColors.GOLD, owner_opt.get().getName(), TextColors.AQUA, " is the owner of this stall until ",
								TextColors.GOLD, date));
					}
					else {
						double cost = stall.getApplicableRentPerWeek();
						Account account = EconomyUtils.getUserAccount(player.getUniqueId());
						TransactionResult result = account.withdraw(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(cost), DumbCompatibilityStuff.source(Bazaar.getInstance()));
						if (result.getResult().equals(ResultType.ACCOUNT_NO_FUNDS)) {
							player.sendMessage(Text.of(TextColors.RED, "Insufficient funds"));
							return;
						}
						if (!result.getResult().equals(ResultType.SUCCESS)) {
							player.sendMessage(Text.of(TextColors.RED, "There was a problem withdrawing from your account"));
							return;
						}
						stall.setAutoRent(false);
						stall.setOwner(player);
						stall.addWeeksOfRent(1);
						stall.showEdges();
						SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
						String date = stall.getEvictionDate().isPresent() ? format.format(stall.getEvictionDate().get()) : "basically forever";
						player.sendMessage(Text.of(TextColors.AQUA, "You are the owner of this stall until ", TextColors.GOLD, date));
					}
					return;
				}
			}
		}
		*/
	}
}
