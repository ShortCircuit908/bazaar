package com.shortcircuit.bazaar.command;

import com.flowpowered.math.vector.Vector3i;
import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.listeners.MarketListener;
import com.shortcircuit.bazaar.market.Market;
import com.shortcircuit.bazaar.market.MarketStall;
import com.shortcircuit.bazaar.selection.Selectors;
import com.shortcircuit.bazaar.util.DumbCompatibilityStuff;
import com.shortcircuit.bazaar.util.EconomyUtils;
import com.shortcircuit.bazaar.util.VectorUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.Optional;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.service.economy.account.Account;
import org.spongepowered.api.service.economy.transaction.ResultType;
import org.spongepowered.api.service.economy.transaction.TransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * @author ShortCircuit908
 * Created on 1/9/2018.
 */
public class StallCommand {
	public StallCommand(Bazaar plugin) {
		CommandSpec spec = CommandSpec.builder()
				.description(Text.of("Manage shop stalls"))
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.create")
								.description(Text.of("Create a new stall from a selection"))
								.executor(new Create())
								.build(),
						"create", "new"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.remove")
								.description(Text.of("Remove a stall at your location"))
								.executor(new Remove())
								.build(),
						"remove", "delete"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.setrent")
								.description(Text.of("Set the rent of a stall at your location"))
								.arguments(
										GenericArguments.optional(
												GenericArguments.firstParsing(
														GenericArguments.literal(Text.of("default"), "default"),
														GenericArguments.doubleNum(Text.of("rent"))
												)
										)
								)
								.executor(new SetRent())
								.build(),
						"setrent"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")
								.description(Text.of("Get the rent of a stall at your location"))
								.executor(new GetRent())
								.build(),
						"getrent"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")
								.description(Text.of("Get the owner of a stall at your location"))
								.executor(new Owner())
								.build(),
						"owner", "getowner", "available"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")
								.description(Text.of("Rent a stall at your location"))
								.arguments(
										GenericArguments.optional(
												GenericArguments.integer(Text.of("weeks"))
										)
								)
								.executor(new Rent())
								.build(),
						"rent", "lease", "addrent"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")
								.description(Text.of("Toggle automatic rent payment of a stall at your location"))
								.executor(new AutoRent())
								.build(),
						"autorent"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.rent")
								.description(Text.of("Stop renting a stall at your location"))
								.executor(new UnRent())
								.build(),
						"unrent", "unlease"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.evict")
								.description(Text.of("Evict a player from a stall at your location"))
								.executor(new Evict())
								.build(),
						"evict"
				)
				.child(
						CommandSpec.builder()
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.stall.setsign")
								.description(Text.of("Set the info sign of a stall at your location"))
								.executor(new SetSign())
								.build(),
						"sign", "setsign"
				)
				.build();
		Sponge.getCommandManager().register(plugin, spec, "stall");
	}
	
	private class Create implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Selectors.initSelector((Player) src, new MarketStall());
			return CommandResult.success();
		}
	}
	
	private class Remove implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			for (Market market : Market.getMarkets()) {
				if (!market.getWorld().isPresent()) {
					continue;
				}
				if (!player.getWorld().getUniqueId().equals(market.getWorld().get().getUniqueId())) {
					continue;
				}
				Iterator<MarketStall> stall_iterator = market.getStalls().iterator();
				while (stall_iterator.hasNext()) {
					MarketStall stall = stall_iterator.next();
					Pair<Vector3i, Vector3i> normalized = stall.getNormalizedPoints();
					if (normalized == null || normalized.getLeft() == null || normalized.getRight() == null) {
						stall_iterator.remove();
						continue;
					}
					if (VectorUtils.isPointInsideVolume(player.getLocation().getBlockPosition(), normalized.getLeft(), normalized.getRight())) {
						stall_iterator.remove();
						src.sendMessage(Text.of(TextColors.AQUA, "Removed stall at your location"));
						return CommandResult.success();
					}
				}
			}
			throw new CommandException(Text.of("There is no stall at your location"));
		}
	}
	
	private class SetRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Optional<Double> rent_opt = args.getOne("rent");
			Optional<MarketStall> stall_opt = MarketStall.getStall(((Player) src).getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			stall.showEdges();
			if (args.hasAny("default")) {
				stall.setRentPerWeek(null);
				src.sendMessage(Text.of(TextColors.AQUA, "The weekly rent of this stall has been set to the market default"));
				return CommandResult.success();
			}
			if (rent_opt.isPresent()) {
				stall.setRentPerWeek(rent_opt.get());
				src.sendMessage(Text.of(TextColors.AQUA, "Set this stall's weekly rent to ", TextColors.GOLD,
						EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(rent_opt.get()))));
			}
			return CommandResult.success();
		}
	}
	
	private class GetRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			stall.showEdges();
			src.sendMessage(Text.of(TextColors.AQUA, "The weekly rent of this stall is ", TextColors.GOLD,
					EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(stall.getApplicableRentPerWeek()))));
			return CommandResult.success();
		}
	}
	
	private static class Owner implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			Optional<User> owner_opt = stall.getOwner();
			if (owner_opt.isPresent()) {
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				String date = stall.getEvictionDate().isPresent() ? format.format(stall.getEvictionDate().get()) : "forever";
				if (owner_opt.get().getUniqueId().equals(player.getUniqueId())) {
					stall.showEdges();
					src.sendMessages(
							Text.of(TextColors.AQUA, "You are the owner of this stall until ", TextColors.GOLD, date),
							Text.of(TextColors.AQUA, "Use ", TextColors.GOLD, "/stall rent [weeks]", TextColors.AQUA, " to extend your lease")
					);
				}
				else {
					src.sendMessage(Text.of(TextColors.GOLD, owner_opt.get().getName(), TextColors.AQUA, " is the owner of this stall until ",
							TextColors.GOLD, date));
				}
			}
			else {
				stall.showEdges();
				src.sendMessage(Text.of(TextColors.AQUA, "This stall is available to rent for ", TextColors.GOLD,
						EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(stall.getApplicableRentPerWeek())),
						TextColors.AQUA, " per week"));
			}
			return CommandResult.success();
		}
	}
	
	private static class Rent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			Optional<User> owner_opt = stall.getOwner();
			if (owner_opt.isPresent() && !owner_opt.get().getUniqueId().equals(player.getUniqueId())) {
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				String date = stall.getEvictionDate().isPresent() ? format.format(stall.getEvictionDate().get()) : "basically forever";
				src.sendMessage(Text.of(TextColors.GOLD, owner_opt.get().getName(), TextColors.AQUA, " is the owner of this stall until ",
						TextColors.GOLD, date));
			}
			else {
				int weeks = args.<Integer>getOne("weeks").orElse(1);
				double cost = stall.getApplicableRentPerWeek() * weeks;
				Account account = EconomyUtils.getUserAccount(player.getUniqueId());
				TransactionResult result = account.withdraw(EconomyUtils.getDefaultCurrency(), BigDecimal.valueOf(cost), DumbCompatibilityStuff.source(Bazaar.getInstance()));
				if (result.getResult().equals(ResultType.ACCOUNT_NO_FUNDS)) {
					throw new CommandException(Text.of("Insufficient funds"));
				}
				if (!result.getResult().equals(ResultType.SUCCESS)) {
					throw new CommandException(Text.of("There was a problem withdrawing from your account"));
				}
				stall.setAutoRent(false);
				stall.setOwner(player);
				stall.addWeeksOfRent(weeks);
				stall.showEdges();
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				String date = stall.getEvictionDate().isPresent() ? format.format(stall.getEvictionDate().get()) : "basically forever";
				src.sendMessage(Text.of(TextColors.AQUA, "You are the owner of this stall until ", TextColors.GOLD, date));
			}
			return CommandResult.success();
		}
	}
	
	private static class AutoRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			Optional<User> owner_opt = stall.getOwner();
			if (!owner_opt.isPresent() || !owner_opt.get().getUniqueId().equals(player.getUniqueId())) {
				throw new CommandException(Text.of("You are not renting this stall"));
			}
			stall.setAutoRent(!stall.isAutoRent());
			Text state = stall.isAutoRent() ? Text.of(TextColors.GREEN, "true") : Text.of(TextColors.RED, "false");
			src.sendMessage(Text.of(TextColors.AQUA, "Auto-renting of this stall has been set to ", state));
			return CommandResult.success();
		}
	}
	
	private static class Evict implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			Optional<User> owner_opt = stall.getOwner();
			if (!owner_opt.isPresent()) {
				throw new CommandException(Text.of("This stall is not rented by anyone"));
			}
			stall.setEvictionDate(null);
			stall.setOwner(null);
			stall.setAutoRent(false);
			src.sendMessage(Text.of(TextColors.AQUA, "Evicted ", TextColors.GOLD, owner_opt.get().getName(), TextColors.AQUA, " from this stall"));
			return CommandResult.success();
		}
	}
	
	private static class UnRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			Optional<User> owner_opt = stall.getOwner();
			if (!owner_opt.isPresent()) {
				throw new CommandException(Text.of("This stall is not rented by anyone"));
			}
			if (!owner_opt.get().getUniqueId().equals(player.getUniqueId())) {
				throw new CommandException(Text.of("You are not the owner of this stall"));
			}
			stall.setEvictionDate(null);
			stall.setOwner(null);
			stall.setAutoRent(false);
			src.sendMessage(Text.of(TextColors.AQUA, "Stall unrented"));
			return CommandResult.success();
		}
	}
	
	private static class SetSign implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			Player player = (Player) src;
			Optional<MarketStall> stall_opt = MarketStall.getStall(player.getLocation());
			if (!stall_opt.isPresent()) {
				throw new CommandException(Text.of("There is no stall at your location"));
			}
			MarketStall stall = stall_opt.get();
			player.sendMessages(
					Text.of(TextColors.AQUA, "Right-click a sign to set the info sign"),
					Text.of(TextColors.AQUA, "Right-click any other block to cancel")
			);
			MarketListener.stall_sign_selectors.put(player.getUniqueId(), stall);
			return CommandResult.success();
		}
	}
}
