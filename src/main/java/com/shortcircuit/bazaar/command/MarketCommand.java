package com.shortcircuit.bazaar.command;

import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.market.Market;
import com.shortcircuit.bazaar.market.MarketStall;
import com.shortcircuit.bazaar.selection.Selectors;
import com.shortcircuit.bazaar.util.EconomyUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

/**
 * @author ShortCircuit908
 * Created on 1/9/2018.
 */
public class MarketCommand {
	public MarketCommand(Bazaar plugin) {
		CommandSpec spec = CommandSpec.builder()
				.description(Text.of("Manage markets"))
				.child(
						CommandSpec.builder()
								.description(Text.of("Create a new market from a selection"))
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.market.create")
								.executor(new Create())
								.arguments(
										GenericArguments.string(Text.of("name"))
								)
								.build(),
						"create", "new"
				)
				.child(
						CommandSpec.builder()
								.description(Text.of("Set the default rent of a market"))
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.market.setrent")
								.executor(new SetRent())
								.arguments(
										GenericArguments.choices(
												Text.of("name"),
												() -> {
													ArrayList<String> names = new ArrayList<>(Market.getMarkets().size());
													for (Market market : Market.getMarkets()) {
														names.add(market.getName());
													}
													return names;
												},
												s -> s
										),
										GenericArguments.doubleNum(Text.of("rent"))
								)
								.build(),
						"setrent", "setdefaultrent"
				)
				.child(
						CommandSpec.builder()
								.description(Text.of("Get the default rent of a market"))
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.market.setrent")
								.executor(new GetRent())
								.arguments(
										GenericArguments.choices(
												Text.of("name"),
												() -> {
													ArrayList<String> names = new ArrayList<>(Market.getMarkets().size());
													for (Market market : Market.getMarkets()) {
														names.add(market.getName());
													}
													return names;
												},
												s -> s
										)
								)
								.build(),
						"getrent", "getdefaultrent"
				)
				.child(
						CommandSpec.builder()
								.description(Text.of("Remove a market"))
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.market.remove")
								.executor(new Remove())
								.arguments(
										GenericArguments.choices(
												Text.of("name"),
												() -> {
													ArrayList<String> names = new ArrayList<>(Market.getMarkets().size());
													for (Market market : Market.getMarkets()) {
														names.add(market.getName());
													}
													return names;
												},
												s -> s
										)
								)
								.build(),
						"remove", "delete"
				)
				.child(
						CommandSpec.builder()
								.description(Text.of("List all markets"))
								.permission(Bazaar.PLUGIN_IDENTIFIER + ".command.market.list")
								.executor(new List())
								.build(),
						"list"
				)
				//.executor(this)
				.build();
		Sponge.getCommandManager().register(plugin, spec, "market");
	}
	
	private class Create implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			if (!(src instanceof Player)) {
				throw new CommandException(Text.of("This command is player-only"));
			}
			String name = args.<String>getOne("name").get();
			for (Market market : Market.getMarkets()) {
				if (market.getName().equalsIgnoreCase(name)) {
					throw new CommandException(Text.of("A market with that name already exists"));
				}
			}
			Selectors.initSelector((Player) src, new Market(name));
			return CommandResult.success();
		}
	}
	
	private class SetRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			String name = args.<String>getOne("name").get();
			Double rent = args.<Double>getOne("rent").get();
			for (Market market : Market.getMarkets()) {
				if (market.getName().equalsIgnoreCase(name)) {
					market.setDefaultRentPerWeek(rent);
					src.sendMessage(Text.of(TextColors.AQUA, "Set the default rent for ", TextColors.GOLD, market.getName(),
							TextColors.AQUA, " to ", TextColors.GOLD, EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(market.getDefaultRentPerWeek()))));
					for (MarketStall stall : market.getStalls()) {
						stall.updateInfoSign();
					}
					return CommandResult.success();
				}
			}
			throw new CommandException(Text.of("No market with that name exists"));
		}
	}
	
	private class GetRent implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			String name = args.<String>getOne("name").get();
			for (Market market : Market.getMarkets()) {
				if (market.getName().equalsIgnoreCase(name)) {
					src.sendMessage(Text.of(TextColors.AQUA, "The default rent for ", TextColors.GOLD, market.getName(),
							TextColors.AQUA, " is ", TextColors.GOLD, EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(market.getDefaultRentPerWeek()))));
				}
				return CommandResult.success();
			}
			throw new CommandException(Text.of("No market with that name exists"));
		}
	}
	
	private class Remove implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			String name = args.<String>getOne("name").get();
			Iterator<Market> market_iterator = Market.getMarkets().iterator();
			boolean removed = false;
			while (market_iterator.hasNext()) {
				Market market = market_iterator.next();
				if (market.getName().equalsIgnoreCase(name)) {
					market_iterator.remove();
					src.sendMessage(Text.of(TextColors.AQUA, "Removed market: ", TextColors.GOLD, market.getName()));
					removed = true;
				}
			}
			if (!removed) {
				throw new CommandException(Text.of("No market with that name exists"));
			}
			return CommandResult.success();
		}
	}
	
	private class List implements CommandExecutor {
		@Override
		public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {
			ArrayList<Market> markets = new ArrayList<>(Market.getMarkets());
			int i = 0;
			ArrayList<Text> texts = new ArrayList<>(markets.size() + 1);
			texts.add(Text.of(TextColors.AQUA, "Showing ", TextColors.GOLD, markets.size(), TextColors.AQUA, " market" + (markets.size() == 1 ? "" : "s")));
			for (Market market : markets) {
				int count = 0;
				for (MarketStall stall : market.getStalls()) {
					if (!stall.getOwner().isPresent()) {
						count++;
					}
				}
				Text count_text = count == 0 ? Text.of(TextColors.RED, "No available stalls")
						: Text.of(TextColors.GOLD, count, TextColors.AQUA, " stall" + (count == 1 ? "" : "s") + " available to rent");
				Text name_text = Text.builder(market.getName())
						.color(TextColors.GOLD)
						.style(src instanceof Player ? TextStyles.UNDERLINE : TextStyles.NONE)
						.onHover(
								TextActions.showText(
										count_text
								)
						)
						.build();
				texts.add(Text.of(TextColors.AQUA, ++i, ". ", name_text));
			}
			src.sendMessages(texts);
			return CommandResult.success();
		}
	}
}
