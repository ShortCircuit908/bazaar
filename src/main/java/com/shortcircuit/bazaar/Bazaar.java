package com.shortcircuit.bazaar;

import com.google.common.collect.ImmutableMap;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import com.shortcircuit.bazaar.command.MarketCommand;
import com.shortcircuit.bazaar.command.StallCommand;
import com.shortcircuit.bazaar.event.DoubleClickBlockEvent;
import com.shortcircuit.bazaar.listeners.MarketListener;
import com.shortcircuit.bazaar.listeners.ShopListener;
import com.shortcircuit.bazaar.market.EvictionTask;
import com.shortcircuit.bazaar.market.Market;
import com.shortcircuit.bazaar.market.MarketStall;
import com.shortcircuit.bazaar.selection.Selectors;
import com.shortcircuit.bazaar.util.SerializerUtils;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.scheduler.Task;

/**
 * @author ShortCircuit908
 * Created on 1/8/2018.
 */
@Plugin(
		id = Bazaar.PLUGIN_IDENTIFIER,
		name = "Bazaar",
		version = "1.11-SNAPSHOT",
		description = "Full market solution",
		authors = {"ShortCircuit908"}
)
public class Bazaar {
	public static final String PLUGIN_IDENTIFIER = "bazaar";
	private static Bazaar instance;
	private Config config = new Config();
	private Task visualizer_task;
	private Task eviction_task;
	
	@Inject
	private Logger logger;
	
	@Inject
	@ConfigDir(sharedRoot = false)
	private Path config_path;
	
	@Inject
	@DefaultConfig(sharedRoot = false)
	private File default_config;
	
	@Inject
	@DefaultConfig(sharedRoot = false)
	private ConfigurationLoader<CommentedConfigurationNode> config_loader;
	private ConfigurationLoader<CommentedConfigurationNode> market_loader;
	
	private CommentedConfigurationNode config_node;
	private ConfigurationNode market_node;
	
	public Bazaar() {
		instance = this;
	}
	
	@Listener
	public void onInitialize(final GameInitializationEvent event){
		if (market_loader == null) {
			market_loader = HoconConfigurationLoader.builder()
					.setFile(config_path.resolve("markets.conf").toFile())
					.build();
			market_loader.getDefaultOptions().setSerializers(SerializerUtils.registerStandardSerialziers(market_loader.getDefaultOptions().getSerializers()));
		}
		try {
			loadConfig();
			if (config.selection_wand_type == null) {
				config.selection_wand_type = Config.DEFAULT.selection_wand_type;
			}
			saveConfig();
		}
		catch (IOException | ObjectMappingException e) {
			e.printStackTrace();
		}
		try {
			loadMarkets();
		}
		catch (IOException | ObjectMappingException e) {
			e.printStackTrace();
		}
		Selectors.EdgeVisualizer visualizer = new Selectors.EdgeVisualizer();
		Sponge.getEventManager().registerListeners(this, new ShopListener());
		Sponge.getEventManager().registerListeners(this, new DoubleClickBlockEvent.ClickListener());
		Sponge.getEventManager().registerListeners(this, new MarketListener());
		Sponge.getEventManager().registerListeners(this, visualizer);
		visualizer_task = Sponge.getScheduler().createTaskBuilder()
				.execute(visualizer)
				.intervalTicks(20)
				.delayTicks(20)
				.async()
				.submit(this);
	}
	
	@Listener
	public void onServerStarting(final GameStartingServerEvent event) {
		new StallCommand(this);
		new MarketCommand(this);
		eviction_task = Sponge.getScheduler().createTaskBuilder()
				.execute(new EvictionTask())
				.interval(10, TimeUnit.MINUTES)
				.delayTicks(20)
				.async()
				.submit(this);
	}
	
	@Listener
	public void onServerStopping(final GameStoppingServerEvent event) {
		if (visualizer_task != null) {
			visualizer_task.cancel();
		}
		if (eviction_task != null) {
			eviction_task.cancel();
		}
		try {
			saveMarkets();
		}
		catch (IOException | ObjectMappingException e) {
			e.printStackTrace();
		}
	}
	
	public void loadConfig() throws IOException, ObjectMappingException {
		config_node = config_loader.load();
		config = config_node.getValue(TypeToken.of(Config.class), Config.DEFAULT);
		getLogger().info("Configuration loaded");
	}
	
	public void saveConfig() throws IOException, ObjectMappingException {
		config_node.setValue(TypeToken.of(Config.class), config);
		config_loader.save(config_node);
		getLogger().info("Configuration saved");
	}
	
	public void loadMarkets() throws IOException, ObjectMappingException {
		market_node = market_loader.load();
		TypeToken<Map<String, List<Market>>> type = new TypeToken<Map<String, List<Market>>>() {
		};
		Map<String, List<Market>> map = market_node.getValue(type, new HashMap<>());
		List<Market> markets = map.computeIfAbsent("markets", (key) -> new ArrayList<>());
		for (Market market : markets) {
			for (MarketStall stall : market.getStalls()) {
				stall.setMarket(market);
			}
		}
		Market.getMarkets().clear();
		Market.getMarkets().addAll(markets);
		getLogger().info("Markets loaded");
	}
	
	public void saveMarkets() throws IOException, ObjectMappingException {
		Map<String, List<Market>> map = ImmutableMap.of("markets", new ArrayList<>(Market.getMarkets()));
		TypeToken<Map<String, List<Market>>> type = new TypeToken<Map<String, List<Market>>>() {
		};
		market_node.setValue(type, map);
		market_loader.save(market_node);
		logger.info("Markets saved");
	}
	
	public Config getConfig() {
		return config;
	}
	
	public Logger getLogger() {
		return logger;
	}
	
	public static Bazaar getInstance() {
		return instance;
	}
	
	@ConfigSerializable
	public static class Config {
		public static final Config DEFAULT = new Config();
		@Setting(value = "selection-wand-type", comment = "Item type of the market selection wand")
		public String selection_wand_type = "minecraft:golden_pickaxe";
		@Setting(value = "use-server-money", comment = "If set to \"true\" all admin shops will transact with a global server bank account")
		public boolean use_server_money = false;
	}
}
