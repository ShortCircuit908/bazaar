package com.shortcircuit.bazaar.util;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.service.ProvisioningException;
import org.spongepowered.api.service.economy.Currency;
import org.spongepowered.api.service.economy.EconomyService;
import org.spongepowered.api.service.economy.account.Account;

/**
 * @author ShortCircuit908
 * Created on 1/11/2018.
 */
public class EconomyUtils {
	public static final String SERVER_ACCOUNT = "{server}";
	private static EconomyService economy_service;
	
	public static Account getServerAccount() {
		ensureEconomyService();
		return economy_service.getOrCreateAccount(SERVER_ACCOUNT).get();
	}
	
	public static Account getUserAccount(UUID uuid) {
		ensureEconomyService();
		return economy_service.getOrCreateAccount(uuid).get();
	}
	
	public static Set<Currency> getCurrencies() {
		ensureEconomyService();
		return economy_service.getCurrencies();
	}
	
	public static Currency getDefaultCurrency() {
		ensureEconomyService();
		return economy_service.getDefaultCurrency();
	}
	
	private static void ensureEconomyService() {
		if (economy_service == null) {
			Optional<EconomyService> economy_service_opt = Sponge.getServiceManager().provide(EconomyService.class);
			economy_service = economy_service_opt.orElseThrow(() -> new ProvisioningException(EconomyService.class));
		}
	}
}
