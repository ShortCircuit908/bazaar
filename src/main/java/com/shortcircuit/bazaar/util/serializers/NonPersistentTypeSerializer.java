package com.shortcircuit.bazaar.util.serializers;

import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 1/16/2018.
 */
public interface NonPersistentTypeSerializer<T> extends TypeSerializer<T> {
}
