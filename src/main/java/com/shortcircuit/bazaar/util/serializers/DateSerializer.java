package com.shortcircuit.bazaar.util.serializers;

import com.google.common.reflect.TypeToken;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 1/16/2018.
 */
public class DateSerializer implements TypeSerializer<Date> {
	private static final DateFormat full_format = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL);
	private static final DateFormat[] all_formats;
	
	static {
		int[] formats = {DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT};
		all_formats = new DateFormat[formats.length * formats.length + formats.length];
		int i = 0;
		for (int date_format : formats) {
			for (int time_format : formats) {
				all_formats[i++] = DateFormat.getDateTimeInstance(date_format, time_format);
			}
			all_formats[i++] = DateFormat.getDateInstance(date_format);
		}
	}
	
	@Override
	public Date deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		if (value.getString() == null) {
			return null;
		}
		Throwable thrown = null;
		for (DateFormat format : all_formats) {
			try {
				return format.parse(value.getString());
			}
			catch (ParseException e) {
				thrown = e;
			}
		}
		if (thrown != null) {
			throw new ObjectMappingException(thrown);
		}
		throw new ObjectMappingException("Unable to parse date");
	}
	
	@Override
	public void serialize(TypeToken<?> type, Date obj, ConfigurationNode value) {
		if (obj == null) {
			value.setValue(null);
		}
		else {
			value.setValue(full_format.format(obj));
		}
	}
}
