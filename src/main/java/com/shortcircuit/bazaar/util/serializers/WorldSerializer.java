package com.shortcircuit.bazaar.util.serializers;

import com.google.common.reflect.TypeToken;
import java.util.UUID;
import ninja.leaping.configurate.ConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/16/2018.
 */
public class WorldSerializer implements NonPersistentTypeSerializer<World> {
	@Override
	public World deserialize(TypeToken<?> type, ConfigurationNode value) {
		if (value.getString() == null) {
			return null;
		}
		try {
			UUID world_id = UUID.fromString(value.getString());
			return Sponge.getServer().getWorld(world_id).orElse(null);
		}
		catch (IllegalArgumentException e) {
			// Do nothing
		}
		return Sponge.getServer().getWorld(value.getString()).orElse(null);
	}
	
	@Override
	public void serialize(TypeToken<?> type, World obj, ConfigurationNode value) {
		if (obj == null) {
			value.setValue(null);
		}
		else {
			value.setValue(obj.getName());
		}
	}
}
