package com.shortcircuit.bazaar.util.serializers;

import com.google.common.reflect.TypeToken;
import com.shortcircuit.bazaar.util.UserUtils;
import java.util.UUID;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializers;
import org.spongepowered.api.entity.living.player.User;

/**
 * @author ShortCircuit908
 * Created on 1/16/2018.
 */
public class UserSerializer implements NonPersistentTypeSerializer<User> {
	@Override
	public User deserialize(TypeToken<?> type, ConfigurationNode value) {
		if (value.getString() == null) {
			return null;
		}
		try {
			TypeSerializer<UUID> uuid_serializer = TypeSerializers.getDefaultSerializers().get(TypeToken.of(UUID.class));
			UUID player_id = uuid_serializer.deserialize(TypeToken.of(UUID.class), value);
			return UserUtils.getUser(player_id).orElse(null);
		}
		catch (ObjectMappingException e) {
			// Do nothing
		}
		return UserUtils.getUser(value.getString()).orElse(null);
	}
	
	@Override
	public void serialize(TypeToken<?> type, User obj, ConfigurationNode value) {
		if (obj == null) {
			value.setValue(null);
		}
		else {
			value.setValue(obj.getUniqueId().toString());
		}
	}
}
