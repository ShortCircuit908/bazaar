package com.shortcircuit.bazaar.util;

import java.lang.reflect.Method;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.type.GridInventory;

/**
 * @author ShortCircuit908
 * Created on 1/14/2018.
 */
public class DumbCompatibilityStuff {
	public static GridInventory getMainInventoy(PlayerInventory inventory) {
		try {
			Method method_PlayerInventory__getMain = PlayerInventory.class.getDeclaredMethod("getMain");
			return (GridInventory) method_PlayerInventory__getMain.invoke(inventory);
		}
		catch (ReflectiveOperationException e) {
			// Do nothing
		}
		return inventory.getMainGrid();
	}
	
	public static Cause source(Object source) {
		try {
			Method method_Cause__source = Cause.class.getDeclaredMethod("source", Object.class);
			Method method_Cause$Builder__build = Cause.Builder.class.getDeclaredMethod("build");
			Cause.Builder builder = (Cause.Builder) method_Cause__source.invoke(null, source);
			return (Cause) method_Cause$Builder__build.invoke(builder);
		}
		catch (ReflectiveOperationException e) {
			// Do nothing
		}
		return Cause.builder().append(source).build(org.spongepowered.api.event.cause.EventContext.empty());
	}
}
