package com.shortcircuit.bazaar.util;

import com.google.common.collect.ImmutableMap;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Map;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.TextElement;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.text.format.TextStyles;

/**
 * @author ShortCircuit908
 * Created on 1/22/2018.
 */
public class TextUtils {
	private static final Map<Character, TextElement[]> format_codes = ImmutableMap.<Character, TextElement[]>builder()
			.put('0', new TextElement[]{TextColors.BLACK})
			.put('1', new TextElement[]{TextColors.DARK_BLUE})
			.put('2', new TextElement[]{TextColors.DARK_GREEN})
			.put('3', new TextElement[]{TextColors.DARK_AQUA})
			.put('4', new TextElement[]{TextColors.DARK_RED})
			.put('5', new TextElement[]{TextColors.DARK_PURPLE})
			.put('6', new TextElement[]{TextColors.GOLD})
			.put('7', new TextElement[]{TextColors.GRAY})
			.put('8', new TextElement[]{TextColors.DARK_GRAY})
			.put('9', new TextElement[]{TextColors.BLUE})
			.put('a', new TextElement[]{TextColors.GREEN})
			.put('b', new TextElement[]{TextColors.AQUA})
			.put('c', new TextElement[]{TextColors.RED})
			.put('d', new TextElement[]{TextColors.LIGHT_PURPLE})
			.put('e', new TextElement[]{TextColors.YELLOW})
			.put('f', new TextElement[]{TextColors.WHITE})
			.put('k', new TextElement[]{TextStyles.OBFUSCATED})
			.put('l', new TextElement[]{TextStyles.BOLD})
			.put('m', new TextElement[]{TextStyles.STRIKETHROUGH})
			.put('n', new TextElement[]{TextStyles.UNDERLINE})
			.put('o', new TextElement[]{TextStyles.ITALIC})
			.put('r', new TextElement[]{TextColors.RESET, TextStyles.RESET})
			.build();
	
	public static Text translateAlternateFormattingCodes(char format_code, Text text) {
		if (text == null) {
			return null;
		}
		Text.Builder builder = translateAlternateFormattingCodes(format_code, text.toPlainSingle()).toBuilder();
		copyActions(text, builder);
		for (Text child : text.getChildren()) {
			builder.append(translateAlternateFormattingCodes(format_code, child));
		}
		return builder.build();
	}
	
	public static Text.Builder copyActions(Text source, Text.Builder dest) {
		return copyActions(source, dest, false);
	}
	
	public static Text.Builder copyActions(Text source, Text.Builder dest, boolean ignore_absent) {
		return dest.onHover(source.getHoverAction().orElse(ignore_absent ? dest.getHoverAction().orElse(null) : null))
				.onClick(source.getClickAction().orElse(ignore_absent ? dest.getClickAction().orElse(null) : null))
				.onShiftClick(source.getShiftClickAction().orElse(ignore_absent ? dest.getShiftClickAction().orElse(null) : null));
	}
	
	public static Text copyActions(Text souce, Text dest) {
		return copyActions(souce, dest, false);
	}
	
	public static Text copyActions(Text source, Text dest, boolean ignore_absent) {
		return copyActions(source, dest.toBuilder(), ignore_absent).build();
	}
	
	public static Text translateAlternateFormattingCodes(char format_code, String text) {
		if (text == null) {
			return null;
		}
		LinkedList<Object> objects = new LinkedList<>();
		StringBuilder cur_string = new StringBuilder(text.length());
		for (int i = 0; i < text.length(); i++) {
			char cur_char = text.charAt(i);
			if (i == text.length() - 1) {
				cur_string.append(cur_char);
				continue;
			}
			if (cur_char != format_code) {
				cur_string.append(cur_char);
				continue;
			}
			char peek = text.charAt(i + 1);
			if (!format_codes.containsKey(Character.toLowerCase(peek))) {
				cur_string.append(cur_char);
				continue;
			}
			if (cur_string.length() > 0) {
				objects.add(cur_string.toString());
				cur_string = new StringBuilder(text.length() - i);
			}
			i++;
			objects.addAll(Arrays.asList(format_codes.get(peek)));
		}
		if (cur_string.length() > 0) {
			objects.add(cur_string.toString());
		}
		Object[] objects_array = objects.toArray();
		return Text.of(objects_array);
	}
}
