package com.shortcircuit.bazaar.util;

import com.flowpowered.math.vector.Vector3i;

/**
 * @author ShortCircuit908
 * Created on 1/14/018.
 */
public class VectorUtils {
	public static boolean isVolumeEncased(Vector3i min1, Vector3i max1, Vector3i min2, Vector3i max2) {
		return isPointInsideVolume(min1, min2, max2) && isPointInsideVolume(max1, min2, max2);
	}
	
	public static boolean isPointInsideVolume(Vector3i point, Vector3i min, Vector3i max) {
		return point.getX() >= min.getX()
				&& point.getX() <= max.getX()
				&& point.getY() >= min.getY()
				&& point.getY() <= max.getY()
				&& point.getZ() >= min.getZ()
				&& point.getZ() <= max.getZ();
	}
}
