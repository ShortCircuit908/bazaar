package com.shortcircuit.bazaar.util;

import com.shortcircuit.bazaar.Bazaar;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.spongepowered.api.CatalogType;
import org.spongepowered.api.data.key.Key;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.data.value.BaseValue;
import org.spongepowered.api.item.inventory.BlockCarrier;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.item.inventory.Slot;
import org.spongepowered.api.item.inventory.entity.PlayerInventory;
import org.spongepowered.api.item.inventory.type.CarriedInventory;
import org.spongepowered.api.text.translation.Translatable;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/11/2018.
 */
public class ItemUtils {
	public static <T extends CatalogType & Translatable> Optional<T> getVariant(ItemStackSnapshot item) {
		return Optional.ofNullable(getVariantRaw(item));
	}
	
	@SuppressWarnings("unchecked")
	private static <T extends CatalogType & Translatable, E> T getVariantRaw(ItemStackSnapshot item) {
		for (Field field : Keys.class.getDeclaredFields()) {
			if (field.getName().endsWith("_TYPE")) {
				try {
					Key<? extends BaseValue<E>> key = (Key<BaseValue<E>>) field.get(null);
					Optional<E> value_opt = item.get(key);
					if (value_opt.isPresent()) {
						Object value = value_opt.get();
						if (value instanceof CatalogType && value instanceof Translatable) {
							return (T) value;
						}
					}
				}
				catch (ReflectiveOperationException e) {
					// Do nothing
				}
			}
		}
		return null;
	}
	
	private static Iterable<Slot> getSlots(Inventory inventory) {
		if (inventory == null) {
			return null;
		}
		if (inventory instanceof PlayerInventory) {
			List<Slot> slots = new ArrayList<>(inventory.size());
			Iterable<Slot> main_slots = DumbCompatibilityStuff.getMainInventoy((PlayerInventory) inventory).slots();
			Iterable<Slot> hotbar_slots = ((PlayerInventory) inventory).getHotbar().slots();
			main_slots.forEach(slots::add);
			hotbar_slots.forEach(slots::add);
			return slots;
		}
		// TODO: NPE is thrown when the inventory is carried by TileEntityLockable
		try {
			return inventory.slots();
		}
		catch (NullPointerException e) {
			if (inventory instanceof CarriedInventory) {
				Optional<? extends Carrier> carrier_opt = ((CarriedInventory<?>) inventory).getCarrier();
				if (carrier_opt.isPresent()) {
					Carrier carrier = carrier_opt.get();
					if (carrier instanceof BlockCarrier) {
						Location<World> loc = ((BlockCarrier) carrier).getLocation();
						String loc_str = loc.getExtent().getName() + " " + loc.getPosition().toInt().toString();
						Bazaar.getInstance().getLogger().warn("ItemUtils.getSlots() threw NullPointerException for tile entity at " + loc_str);
					}
				}
			}
			return null;
		}
	}
	
	public static void transfer(Collection<ItemStack> items, Inventory source, Inventory destination, boolean allow_nulls) {
		if ((source == null || destination == null) && !allow_nulls) {
			throw new NullPointerException();
		}
		List<ItemStack> transferred = new ArrayList<>(items.size());
		// Removal
		if (source == null) {
			for (ItemStack item : items) {
				transferred.add(item.copy());
			}
		}
		else {
			for (ItemStack item : items) {
				boolean has_data = doesItemHaveData(item);
				int left = item.getQuantity();
				for (Slot slot : getSlots(source)) {
					Optional<ItemStack> slot_item_opt = slot.peek();
					if (slot_item_opt.isPresent()) {
						ItemStack slot_item = slot_item_opt.get();
						if (itemEquals(item, slot_item, !has_data, true)) {
							if (slot_item.getQuantity() <= left) {
								left -= slot_item.getQuantity();
								transferred.add(slot_item.copy());
								slot.poll();
							}
							else {
								int before = slot_item.getQuantity();
								slot_item.setQuantity(slot_item.getQuantity() - left);
								ItemStack copy = slot_item.copy();
								copy.setQuantity(before - slot_item.getQuantity());
								transferred.add(copy);
								left = 0;
								slot.poll();
								slot.offer(slot_item);
							}
						}
					}
					if (left <= 0) {
						break;
					}
				}
			}
		}
		// Insertion
		if (destination != null) {
			for (ItemStack item : transferred) {
				destination.offer(item);
			}
		}
		else {
			transferred.clear();
		}
		// Return rejected items
		if (source != null) {
			for (ItemStack item : transferred) {
				if (item.getQuantity() > 0) {
					source.offer(item);
				}
			}
		}
	}
	
	public static boolean doesItemHaveData(ItemStack item) {
		if (item == null) {
			return false;
		}
		ItemStack dummy_copy = ItemStack.builder()
				.itemType(item.getItem())
				.quantity(item.getQuantity())
				.build();
		return !dummy_copy.equalTo(item);
	}
	
	
	public static ItemStack[] getArrayContents(Inventory inventory) {
		Iterable<Slot> slots = getSlots(inventory);
		int size = 0;
		for (Slot ignored : slots) {
			size++;
		}
		ItemStack[] items = new ItemStack[size];
		int i = 0;
		for (Slot slot : slots) {
			items[i++] = slot.peek().orElse(null);
		}
		return items;
	}
	
	public static int getTotalQuantity(ItemStack item, Inventory inventory) {
		if (inventory == null) {
			return Integer.MAX_VALUE;
		}
		boolean has_data = doesItemHaveData(item);
		Iterable<Slot> slots = getSlots(inventory);
		int count = 0;
		for (Slot slot : slots) {
			Optional<ItemStack> item_opt = slot.peek();
			if (item_opt.isPresent()) {
				ItemStack check_item = item_opt.get();
				if (itemEquals(item, check_item, !has_data, true)) {
					count += check_item.getQuantity();
				}
			}
		}
		return count;
	}
	
	public static boolean doesItemFit(List<ItemStack> items, Inventory inventory) {
		if (inventory == null || items.isEmpty()) {
			return true;
		}
		HashMap<ItemStack, Integer> left = new HashMap<>(items.size());
		for (ItemStack item : items) {
			left.put(item.copy(), item.getQuantity());
		}
		ItemStack[] contents = getArrayContents(inventory);
		for (int i = 0; i < contents.length; i++) {
			ItemStack item = contents[i];
			if (item == null) {
				ItemStack target = items.get(0);
				double max_pcnt_filled = 0.0;
				for (ItemStack check_item : items) {
					double pcnt_filled = (double) check_item.getQuantity() / (double) check_item.getMaxStackQuantity();
					if (pcnt_filled > max_pcnt_filled) {
						target = check_item;
					}
				}
				for (Map.Entry<ItemStack, Integer> entry : left.entrySet()) {
					if (itemDataEquals(entry.getKey(), target)) {
						entry.setValue(entry.getValue() - target.getMaxStackQuantity());
					}
				}
				continue;
			}
			if (item.getQuantity() < item.getMaxStackQuantity()) {
				for (Map.Entry<ItemStack, Integer> entry : left.entrySet()) {
					if (itemDataEquals(entry.getKey(), item)) {
						entry.setValue(entry.getValue() - (item.getMaxStackQuantity() - item.getQuantity()));
					}
				}
			}
		}
		for (Map.Entry<ItemStack, Integer> entry : left.entrySet()) {
			if (entry.getValue() > 0) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean durabilityEquals(ItemStack i1, ItemStack i2) {
		if (i1 == i2) {
			return true;
		}
		if (i1 == null || i2 == null) {
			return false;
		}
		int d1 = 0;
		int d2 = 0;
		Optional<Integer> d1_opt = i1.get(Keys.ITEM_DURABILITY);
		if (d1_opt.isPresent()) {
			d1 = d1_opt.get();
		}
		Optional<Integer> d2_opt = i2.get(Keys.ITEM_DURABILITY);
		if (d2_opt.isPresent()) {
			d2 = d2_opt.get();
		}
		return d1 == d2;
	}
	
	public static boolean itemEquals(ItemStack i1, ItemStack i2, boolean ignore_data, boolean ignore_quantity) {
		if (i1 == i2) {
			return true;
		}
		if (i1 == null || i2 == null) {
			return false;
		}
		if (ignore_quantity) {
			i1 = i1.copy();
			i1.setQuantity(1);
			i2 = i2.copy();
			i2.setQuantity(1);
		}
		if (ignore_data) {
			return i1.getItem().equals(i2.getItem()) && i1.getQuantity() == i2.getQuantity() && durabilityEquals(i1, i2);
		}
		return itemDataEquals(i1, i2);
	}
	
	public static boolean itemDataEquals(ItemStack i1, ItemStack i2) {
		if (i1 == i2) {
			return true;
		}
		if (i1 == null || i2 == null) {
			return false;
		}
		ItemStack c1 = i1.copy();
		c1.setQuantity(1);
		ItemStack c2 = i2.copy();
		c2.setQuantity(1);
		return c1.equalTo(c2);
	}
}
