package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class DoubleArraySerializer implements TypeSerializer<double[]> {
	@Override
	public double[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Double> list = value.getList(TypeToken.of(double.class), (List<Double>) null);
		if (list == null) {
			return null;
		}
		double[] arr = new double[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, double[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Double> list = new ArrayList<>(obj.length);
		for (double elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Double>>() {
		}, list);
	}
}
