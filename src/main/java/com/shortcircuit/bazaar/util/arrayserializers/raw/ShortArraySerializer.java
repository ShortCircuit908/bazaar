package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class ShortArraySerializer implements TypeSerializer<short[]> {
	@Override
	public short[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Short> list = value.getList(TypeToken.of(short.class), (List<Short>) null);
		if (list == null) {
			return null;
		}
		short[] arr = new short[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, short[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Short> list = new ArrayList<>(obj.length);
		for (short elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Short>>() {
		}, list);
	}
}
