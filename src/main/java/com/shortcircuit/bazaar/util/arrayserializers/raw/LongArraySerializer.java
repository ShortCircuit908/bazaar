package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class LongArraySerializer implements TypeSerializer<long[]> {
	@Override
	public long[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Long> list = value.getList(TypeToken.of(long.class), (List<Long>) null);
		if (list == null) {
			return null;
		}
		long[] arr = new long[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, long[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Long> list = new ArrayList<>(obj.length);
		for (long elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Long>>() {
		}, list);
	}
}
