package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class BooleanArraySerializer implements TypeSerializer<boolean[]> {
	@Override
	public boolean[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Boolean> list = value.getList(TypeToken.of(boolean.class), (List<Boolean>) null);
		if (list == null) {
			return null;
		}
		boolean[] arr = new boolean[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, boolean[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Boolean> list = new ArrayList<>(obj.length);
		for (boolean elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Boolean>>() {
		}, list);
	}
}
