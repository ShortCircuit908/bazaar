package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class FloatArraySerializer implements TypeSerializer<float[]> {
	@Override
	public float[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Float> list = value.getList(TypeToken.of(float.class), (List<Float>) null);
		if (list == null) {
			return null;
		}
		float[] arr = new float[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, float[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Float> list = new ArrayList<>(obj.length);
		for (float elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Float>>() {
		}, list);
	}
}
