package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class CharacterSerializer implements TypeSerializer<Character> {
	@Override
	public Character deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		if (value.getString() == null || value.getString().isEmpty()) {
			return null;
		}
		return value.getString().charAt(0);
	}
	
	@Override
	public void serialize(TypeToken<?> type, Character obj, ConfigurationNode value) throws ObjectMappingException {
		if(obj == null){
			value.setValue(null);
			return;
		}
		value.setValue(obj.toString());
	}
}
