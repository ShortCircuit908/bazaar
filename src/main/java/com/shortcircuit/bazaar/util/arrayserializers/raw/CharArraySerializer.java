package com.shortcircuit.bazaar.util.arrayserializers.raw;

import com.google.common.reflect.TypeToken;
import java.util.ArrayList;
import java.util.List;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;

/**
 * @author ShortCircuit908
 * Created on 12/31/2017.
 */
public class CharArraySerializer implements TypeSerializer<char[]> {
	@Override
	public char[] deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
		List<Character> list = value.getList(TypeToken.of(Character.class), (List<Character>) null);
		if (list == null) {
			return null;
		}
		char[] arr = new char[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = list.get(i);
		}
		return arr;
	}
	
	@Override
	public void serialize(TypeToken<?> type, char[] obj, ConfigurationNode value) throws ObjectMappingException {
		if (obj == null) {
			value.setValue(null);
			return;
		}
		List<Character> list = new ArrayList<>(obj.length);
		for (char elem : obj) {
			list.add(elem);
		}
		value.setValue(new TypeToken<List<Character>>() {
		}, list);
	}
}
