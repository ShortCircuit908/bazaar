package com.shortcircuit.bazaar.util;

import com.google.common.reflect.TypeToken;
import com.shortcircuit.bazaar.util.serializers.DateSerializer;
import com.shortcircuit.bazaar.util.serializers.UserSerializer;
import com.shortcircuit.bazaar.util.serializers.WorldSerializer;
import java.util.Date;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializerCollection;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/14/2018.
 */
public class SerializerUtils {
	public static TypeSerializerCollection registerStandardSerialziers(TypeSerializerCollection serializers) {
		serializers.registerType(TypeToken.of(World.class), new WorldSerializer());
		serializers.registerType(TypeToken.of(User.class), new UserSerializer());
		serializers.registerType(TypeToken.of(Date.class), new DateSerializer());
		return serializers;
	}
}
