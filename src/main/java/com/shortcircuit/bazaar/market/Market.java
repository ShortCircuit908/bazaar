package com.shortcircuit.bazaar.market;

import com.flowpowered.math.vector.Vector3i;
import com.shortcircuit.bazaar.selection.ShopSelection;
import com.shortcircuit.bazaar.util.VectorUtils;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/14/2018.
 */
@ConfigSerializable
public class Market extends ShopSelection {
	private static final List<Market> markets = new LinkedList<>();
	
	@Setting("world")
	private String world;
	@Setting("name")
	private String name;
	@Setting("default-rent-per-week")
	private double default_rent_per_week;
	@Setting("stalls")
	private List<MarketStall> stalls = new LinkedList<>();
	
	public Market() {
	
	}
	
	public Market(String name) {
		this.name = name;
	}
	
	public List<MarketStall> getStalls() {
		if (stalls == null) {
			stalls = new LinkedList<>();
		}
		return stalls;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDefaultRentPerWeek(double default_rent_per_week) {
		this.default_rent_per_week = default_rent_per_week;
	}
	
	public double getDefaultRentPerWeek() {
		return default_rent_per_week;
	}
	
	@Override
	public void setWorld(World world) {
		super.setWorld(world);
		this.world = world.getName();
	}
	
	@Override
	public Optional<World> getWorld() {
		return Sponge.getServer().getWorld(world);
	}
	
	@Override
	public String getSelectorName() {
		return "market";
	}
	
	public static List<Market> getMarkets() {
		return markets;
	}
	
	@Override
	public boolean onStopSelection(Player player) {
		Pair<Vector3i, Vector3i> points = getNormalizedPoints();
		if (points.getLeft() == null || points.getRight() == null) {
			player.sendMessages(Text.of(TextColors.RED, "Incomplete selection"));
			return false;
		}
		Vector3i min = points.getLeft();
		Vector3i max = points.getRight();
		// Check if stall is inside a market
		boolean overlapping = false;
		Iterator<Market> market_iterator = Market.getMarkets().iterator();
		while (market_iterator.hasNext()) {
			Market market = market_iterator.next();
			Pair<Vector3i, Vector3i> market_points = market.getNormalizedPoints();
			if (market_points.getLeft() == null || market_points.getRight() == null) {
				market_iterator.remove();
				continue;
			}
			Vector3i market_min = market_points.getLeft();
			Vector3i market_max = market_points.getRight();
			boolean min_inside = VectorUtils.isPointInsideVolume(min, market_min, market_max);
			boolean max_inside = VectorUtils.isPointInsideVolume(max, market_min, market_max);
			if (min_inside || max_inside) {
				overlapping = true;
				market.showEdges(0.5);
			}
		}
		if (overlapping) {
			showEdges();
			player.sendMessages(Text.of(TextColors.RED, "The selection overlaps an existing market"));
			return false;
		}
		Market.getMarkets().add(this);
		return true;
	}
}
