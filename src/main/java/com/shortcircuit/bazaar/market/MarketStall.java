package com.shortcircuit.bazaar.market;

import com.flowpowered.math.vector.Vector3i;
import com.google.common.collect.Lists;
import com.shortcircuit.bazaar.selection.ShopSelection;
import com.shortcircuit.bazaar.util.EconomyUtils;
import com.shortcircuit.bazaar.util.UserUtils;
import com.shortcircuit.bazaar.util.VectorUtils;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import ninja.leaping.configurate.objectmapping.Setting;
import ninja.leaping.configurate.objectmapping.serialize.ConfigSerializable;
import org.apache.commons.lang3.tuple.Pair;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/8/2018.
 */
@ConfigSerializable
public class MarketStall extends ShopSelection {
	@Setting("owner")
	private UUID owner;
	@Setting("rent-paid-until")
	private Date rent_paid_until;
	@Setting("rent-per-week")
	private Double rent_per_week;
	@Setting("auto-rent")
	private boolean auto_rent;
	@Setting("info-sign-position")
	private Vector3i info_sign_position;
	private transient Market market;
	
	public MarketStall() {
	
	}
	
	public Market getMarket() {
		return market;
	}
	
	public void setMarket(Market market) {
		this.market = market;
	}
	
	public Optional<User> getOwner() {
		return UserUtils.getUser(owner);
	}
	
	public void setOwner(User owner) {
		if (this.owner != null && owner != null && !this.owner.equals(owner.getUniqueId())) {
			this.auto_rent = false;
		}
		this.owner = owner == null ? null : owner.getUniqueId();
		updateInfoSign();
	}
	
	public Optional<Date> getEvictionDate() {
		return Optional.ofNullable(rent_paid_until);
	}
	
	public void setEvictionDate(Date eviction_date) {
		this.rent_paid_until = eviction_date;
		updateInfoSign();
	}
	
	public void addWeeksOfRent(int weeks) {
		if (rent_paid_until == null) {
			rent_paid_until = new Date();
		}
		rent_paid_until.setTime(rent_paid_until.getTime() + (ChronoUnit.WEEKS.getDuration().toMillis() * weeks));
		updateInfoSign();
	}
	
	public Optional<Double> getRentPerWeek() {
		return Optional.ofNullable(rent_per_week);
	}
	
	public double getApplicableRentPerWeek() {
		return getRentPerWeek().isPresent() ? getRentPerWeek().get() : market.getDefaultRentPerWeek();
	}
	
	public void setRentPerWeek(Double rent_per_week) {
		this.rent_per_week = rent_per_week;
		updateInfoSign();
	}
	
	public void setAutoRent(boolean auto_rent) {
		this.auto_rent = auto_rent;
	}
	
	public boolean isAutoRent() {
		return auto_rent;
	}
	
	@Override
	public String getSelectorName() {
		return "stall";
	}
	
	@Override
	public Optional<World> getWorld() {
		return market == null ? super.getWorld() : market.getWorld();
	}
	
	@Override
	public boolean onStopSelection(Player player) {
		Pair<Vector3i, Vector3i> points = getNormalizedPoints();
		if (points == null || points.getLeft() == null || points.getRight() == null) {
			player.sendMessages(Text.of(TextColors.RED, "Incomplete selection"));
			return false;
		}
		Vector3i min = points.getLeft();
		Vector3i max = points.getRight();
		// Check if stall is inside a market
		for (Market market : Market.getMarkets()) {
			Pair<Vector3i, Vector3i> market_points = market.getNormalizedPoints();
			if (market_points == null || market_points.getLeft() == null || market_points.getRight() == null) {
				continue;
			}
			Vector3i market_min = market_points.getLeft();
			Vector3i market_max = market_points.getRight();
			boolean min_inside = VectorUtils.isPointInsideVolume(min, market_min, market_max);
			boolean max_inside = VectorUtils.isPointInsideVolume(max, market_min, market_max);
			if (min_inside && max_inside) {
				setMarket(market);
				// Check if stall is overlapping another stall
				// We need every vertex to ensure accuracy
				Vector3i v1 = new Vector3i(min.getX(), min.getY(), max.getZ());
				Vector3i v2 = new Vector3i(min.getX(), max.getY(), min.getZ());
				Vector3i v3 = new Vector3i(min.getX(), max.getY(), max.getZ());
				Vector3i v4 = new Vector3i(max.getX(), min.getY(), max.getZ());
				Vector3i v5 = new Vector3i(max.getX(), max.getY(), min.getZ());
				Vector3i v6 = new Vector3i(max.getX(), max.getY(), max.getZ());
				boolean overlapping = false;
				for (MarketStall stall : market.getStalls()) {
					Pair<Vector3i, Vector3i> stall_points = stall.getNormalizedPoints();
					if (stall_points == null || stall_points.getLeft() == null || stall_points.getRight() == null) {
						continue;
					}
					Vector3i stall_min = stall_points.getLeft();
					Vector3i stall_max = stall_points.getRight();
					if (VectorUtils.isPointInsideVolume(min, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(max, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v1, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v2, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v3, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v4, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v5, stall_min, stall_max)
							|| VectorUtils.isPointInsideVolume(v6, stall_min, stall_max)) {
						overlapping = true;
						stall.showEdges(0.5);
					}
				}
				if (overlapping) {
					showEdges();
					player.sendMessages(Text.of(TextColors.RED, "The selection overlaps an existing stall"));
					return false;
				}
				// Success case
				setMarket(market);
				market.getStalls().add(this);
				return true;
			}
			else if (min_inside != max_inside) {
				player.sendMessages(Text.of(TextColors.RED, "The stall must be completely enclosed by the market"));
				showEdges();
				market.showEdges(0.5);
				return false;
			}
		}
		player.sendMessages(Text.of("The stall must be inside a market"));
		return false;
	}
	
	public static Optional<MarketStall> getStall(Location<World> location) {
		for (Market market : Market.getMarkets()) {
			if (!market.getWorld().isPresent() || !location.getExtent().getUniqueId().equals(market.getWorld().get().getUniqueId())) {
				continue;
			}
			Iterator<MarketStall> stall_iterator = market.getStalls().iterator();
			while (stall_iterator.hasNext()) {
				MarketStall stall = stall_iterator.next();
				Pair<Vector3i, Vector3i> normalized = stall.getNormalizedPoints();
				if (normalized == null || normalized.getLeft() == null || normalized.getRight() == null) {
					stall_iterator.remove();
					continue;
				}
				if (VectorUtils.isPointInsideVolume(location.getBlockPosition(), normalized.getLeft(), normalized.getRight())) {
					return Optional.of(stall);
				}
			}
		}
		return Optional.empty();
	}
	
	public void updateInfoSign() {
		Optional<TileEntity> info_sign = getInfoSign();
		if (info_sign.isPresent()) {
			TileEntity sign = info_sign.get();
			List<Text> lines = Lists.newArrayList(Text.of(), Text.of(), Text.of(), Text.of());
			lines.set(0, Text.of("<", TextColors.DARK_BLUE, "Bazaar", TextColors.RESET, ">"));
			if (getOwner().isPresent()) {
				lines.set(1, Text.of(getOwner().get().getName()));
				lines.set(2, Text.of("Rented until"));
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				String date = getEvictionDate().isPresent() ? format.format(getEvictionDate().get()) : "basically forever";
				lines.set(3, Text.of(date));
			}
			else {
				//lines.set(1, Text.of(TextColors.DARK_GREEN, "Click to rent"));
				lines.set(2, Text.of(EconomyUtils.getDefaultCurrency().format(BigDecimal.valueOf(getApplicableRentPerWeek()))));
				lines.set(3, Text.of("per week"));
			}
			sign.offer(Keys.SIGN_LINES, lines);
		}
	}
	
	public void setInfoSign(TileEntity sign) {
		this.info_sign_position = sign == null ? null : sign.getLocation().getBlockPosition();
		updateInfoSign();
	}
	
	public Optional<Vector3i> getInfoSignPosition() {
		return Optional.ofNullable(info_sign_position);
	}
	
	public Optional<TileEntity> getInfoSign() {
		if (info_sign_position == null || !getWorld().isPresent()) {
			return Optional.empty();
		}
		Optional<TileEntity> tile_entity_opt = getWorld().get().getTileEntity(info_sign_position);
		if (tile_entity_opt.isPresent() && tile_entity_opt.get().supports(Keys.SIGN_LINES)) {
			return tile_entity_opt;
		}
		info_sign_position = null;
		return Optional.empty();
	}
}
