package com.shortcircuit.bazaar.market;

import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.util.DumbCompatibilityStuff;
import com.shortcircuit.bazaar.util.EconomyUtils;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Optional;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.service.economy.account.Account;
import org.spongepowered.api.service.economy.transaction.ResultType;
import org.spongepowered.api.service.economy.transaction.TransactionResult;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

/**
 * @author ShortCircuit908
 * Created on 1/14/2018.
 */
public class EvictionTask implements Runnable {
	@Override
	public void run() {
		Date now = new Date();
		for (Market market : Market.getMarkets()) {
			for (MarketStall stall : market.getStalls()) {
				Optional<User> owner_opt = stall.getOwner();
				if (!owner_opt.isPresent()) {
					continue;
				}
				User owner = owner_opt.get();
				Optional<Instant> last_played_opt = owner.get(Keys.LAST_DATE_PLAYED);
				if (last_played_opt.isPresent()) {
					Instant last_played = last_played_opt.get();
					Instant two_weeks_ago = now.toInstant().minus(14, ChronoUnit.DAYS);
					if (last_played.isBefore(two_weeks_ago) && !owner.hasPermission(Bazaar.PLUGIN_IDENTIFIER + ".noevict")) {
						unrent(stall);
						continue;
					}
				}
				Optional<Date> eviction_opt = stall.getEvictionDate();
				if (eviction_opt.isPresent()) {
					Date eviction_date = eviction_opt.get();
					if (eviction_date.before(now)) {
						if (stall.isAutoRent()) {
							double cost = stall.getApplicableRentPerWeek();
							Account account = EconomyUtils.getUserAccount(owner.getUniqueId());
							TransactionResult result = account.withdraw(EconomyUtils.getDefaultCurrency(),
									BigDecimal.valueOf(cost), DumbCompatibilityStuff.source(Bazaar.getInstance()));
							if (!result.getResult().equals(ResultType.SUCCESS)) {
								unrent(stall);
								continue;
							}
							stall.addWeeksOfRent(1);
						}
						else {
							unrent(stall);
						}
					}
				}
			}
		}
	}
	
	private void unrent(MarketStall stall) {
		Optional<User> owner_opt = stall.getOwner();
		if (owner_opt.isPresent()) {
			User owner = owner_opt.get();
			if (owner.getPlayer().isPresent()) {
				owner.getPlayer().get().sendMessage(Text.of(TextColors.RED, "You have been evicted from one of your stalls in ",
						TextColors.GOLD, stall.getMarket().getName()));
			}
		}
		stall.setAutoRent(false);
		stall.setOwner(null);
		stall.setEvictionDate(null);
	}
}
