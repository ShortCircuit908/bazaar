package com.shortcircuit.bazaar.market;

import com.google.common.collect.Lists;
import com.shortcircuit.bazaar.Bazaar;
import com.shortcircuit.bazaar.util.UserUtils;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.spongepowered.api.block.tileentity.TileEntity;
import org.spongepowered.api.block.tileentity.carrier.TileEntityCarrier;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.entity.living.player.User;
import org.spongepowered.api.item.inventory.Carrier;
import org.spongepowered.api.item.inventory.ItemStackSnapshot;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.action.HoverAction;
import org.spongepowered.api.text.action.TextActions;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.world.LocatableBlock;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

/**
 * @author ShortCircuit908
 * Created on 1/8/2018.
 */
public class ShopSign {
	public static final String PERMISSION_CREATE = Bazaar.PLUGIN_IDENTIFIER + ".shop.create";
	public static final String PERMISSION_CREATE_ADMIN = Bazaar.PLUGIN_IDENTIFIER + ".shop.create.admin";
	public static final String PERMISSION_CREATE_BUY = Bazaar.PLUGIN_IDENTIFIER + ".shop.create.buy";
	public static final String PERMISSION_CREATE_SELL = Bazaar.PLUGIN_IDENTIFIER + ".shop.create.sell";
	
	private static final Pattern buy_pattern = Pattern.compile("s[=:\\s]?(\\d*(\\.\\d+)?)", Pattern.CASE_INSENSITIVE);
	private static final Pattern sell_pattern = Pattern.compile("b[=:\\s]?(\\d*(\\.\\d+)?)", Pattern.CASE_INSENSITIVE);
	private static final Pattern owner_pattern = Pattern.compile("\\[([a-zA-Z0-9_]{3,16})]");
	private final TileEntity sign;
	
	public ShopSign(TileEntity sign, Player owner) {
		this(sign);
		if (!isValid()) {
			return;
		}
		if (!owner.hasPermission(PERMISSION_CREATE)) {
			owner.sendMessage(Text.of(TextColors.RED, "You do not have permission to create a shop"));
			invalidate();
			return;
		}
		if (isAdminShop()) {
			if (!owner.hasPermission(PERMISSION_CREATE_ADMIN)) {
				owner.sendMessage(Text.of(TextColors.RED, "You do not have permission to create an admin shop"));
				invalidate();
				return;
			}
		}
		else {
			setOwner(owner);
		}
		formatSignText();
		if (getBuyPrice().isPresent() && !owner.hasPermission(PERMISSION_CREATE_BUY)) {
			owner.sendMessage(Text.of(TextColors.RED, "You do not have permission to create a buy shop"));
			setBuyPice(null);
		}
		if (getSellPrice().isPresent() && !owner.hasPermission(PERMISSION_CREATE_SELL)) {
			owner.sendMessage(Text.of(TextColors.RED, "You do not have permission to create a sell shop"));
			setSellPrice(null);
		}
		if (!isValid()) {
			invalidate();
		}
	}
	
	public ShopSign(TileEntity sign) {
		this.sign = sign;
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		sign.offer(Keys.SIGN_LINES, lines);
		if (!isValid()) {
			return;
		}
		formatSignText();
	}
	
	public boolean isAdminShop() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		return lines.get(0).toPlain().trim().equalsIgnoreCase("[admin]");
	}
	
	public Optional<User> getOwner() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		Text header = lines.get(0);
		Optional<HoverAction<?>> hover_action_opt = header.getHoverAction();
		if (hover_action_opt.isPresent()) {
			HoverAction<?> hover_action = hover_action_opt.get();
			if (hover_action instanceof HoverAction.ShowEntity) {
				HoverAction.ShowEntity action_show_entity = (HoverAction.ShowEntity) hover_action;
				HoverAction.ShowEntity.Ref ref = action_show_entity.getResult();
				UUID owner_id = ref.getUniqueId();
				return UserUtils.getUser(owner_id);
			}
		}
		Matcher matcher = owner_pattern.matcher(header.toPlain());
		if (matcher.find()) {
			String name = matcher.group(1);
			List<User> possible_owners = UserUtils.getMatchingUsers(name);
			if (possible_owners.size() == 1) {
				return possible_owners.stream().findFirst();
			}
		}
		return Optional.empty();
	}
	
	public void setBuyPice(Double buy_price) {
		setPrices(buy_price, getSellPrice().orElse(null));
	}
	
	public Optional<Double> getBuyPrice() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		Matcher matcher = buy_pattern.matcher(lines.get(3).toPlain());
		if (matcher.find()) {
			try {
				return Optional.of(Double.parseDouble(matcher.group(1)));
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		return Optional.empty();
	}
	
	public void setSellPrice(Double sell_price) {
		setPrices(getBuyPrice().orElse(null), sell_price);
	}
	
	public Optional<Double> getSellPrice() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		Matcher matcher = sell_pattern.matcher(lines.get(3).toPlain());
		if (matcher.find()) {
			try {
				return Optional.of(Double.parseDouble(matcher.group(1)));
			}
			catch (NumberFormatException e) {
				// Do nothing
			}
		}
		return Optional.empty();
	}
	
	private void setPrices(Double buy_price, Double sell_price) {
		Text.Builder price_builder = Text.builder();
		if (buy_price != null) {
			price_builder.append(Text.of(TextColors.DARK_BLUE, "S", TextColors.RESET, priceToString(buy_price)));
		}
		if (sell_price != null) {
			if (buy_price != null) {
				price_builder.append(Text.of(" "));
			}
			price_builder.append(Text.of(TextColors.DARK_BLUE, "B", TextColors.RESET, priceToString(sell_price)));
		}
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		lines.set(3, price_builder.build());
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	public Optional<ItemStackSnapshot> getItem() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		Text item_line = lines.get(1);
		Optional<HoverAction<?>> hover_action_opt = item_line.getHoverAction();
		if (!hover_action_opt.isPresent()) {
			return Optional.empty();
		}
		HoverAction<?> hover_action = hover_action_opt.get();
		if (hover_action instanceof HoverAction.ShowItem) {
			return Optional.of(((HoverAction.ShowItem) hover_action).getResult());
		}
		return Optional.empty();
	}
	
	public void setItem(ItemStackSnapshot item) {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		if (item == null) {
			lines.set(1, Text.of("> ", TextColors.DARK_PURPLE, "ITEM NOT SET", TextColors.RESET, " <"));
		}
		else {
			Text text = Text.builder(item.createStack().getTranslation())
					.onHover(TextActions.showItem(item))
					.build();
			lines.set(1, text);
		}
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	public void setOwner(User owner) {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		Text text = Text.builder("[")
				.append(Text.of(TextColors.DARK_BLUE, owner.getName()))
				.append(Text.of("]"))
				.onHover(TextActions.showEntity(owner.getUniqueId(), owner.getName()))
				.build();
		lines.set(0, text);
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	public void setAdmin() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		lines.set(0, Text.of("[", TextColors.DARK_RED, "Admin", TextColors.RESET, "]"));
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	public int getQuantity() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		String raw_quantity = lines.get(2).toPlain().trim();
		try {
			if (raw_quantity.toLowerCase().startsWith("x")) {
				raw_quantity = raw_quantity.substring(1);
			}
			int quantity = Integer.parseInt(raw_quantity.trim());
			if (quantity > 0) {
				return quantity;
			}
		}
		catch (NumberFormatException e) {
			// Do nothing
		}
		setQuantity(1);
		return 1;
	}
	
	public void setQuantity(int quantity) {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		lines.set(2, Text.of(quantityToString(quantity)));
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	public boolean isOwnerSet() {
		return isAdminShop() || getOwner().isPresent();
	}
	
	public boolean isValid() {
		return sign.supports(Keys.SIGN_LINES)
				&& (sign.getOrElse(Keys.SIGN_LINES, newSignLines()).get(0).toPlain().equals("[?]") || isOwnerSet())
				&& (getBuyPrice().isPresent() || getSellPrice().isPresent());
	}
	
	public boolean isFullyConfigured() {
		return isValid() && isOwnerSet() && isItemSet();
	}
	
	public boolean isItemSet() {
		return getItem().isPresent();
	}
	
	public void invalidate() {
		List<Text> lines = sign.getOrElse(Keys.SIGN_LINES, newSignLines());
		lines.set(0, Text.of("<", TextColors.DARK_BLUE, "Bazaar", TextColors.RESET, ">"));
		lines.set(1, Text.of());
		lines.set(2, Text.of("?"));
		lines.set(3, Text.of());
		sign.offer(Keys.SIGN_LINES, lines);
	}
	
	private List<Text> newSignLines(Text... lines) {
		ArrayList<Text> list = Lists.newArrayList(
				Text.of(),
				Text.of(),
				Text.of(),
				Text.of()
		);
		for (int i = 0; i < Math.min(lines.length, 4); i++) {
			if (lines[i] != null) {
				list.set(i, lines[i]);
			}
		}
		return list;
	}
	
	public void formatSignText() {
		if (!isValid()) {
			return;
		}
		if (isAdminShop()) {
			setAdmin();
		}
		else if (getOwner().isPresent()) {
			setOwner(getOwner().get());
		}
		if (getItem().isPresent()) {
			setItem(getItem().get());
		}
		else {
			setItem(null);
		}
		setQuantity(getQuantity());
		setPrices(getBuyPrice().orElse(null), getSellPrice().orElse(null));
	}
	
	public Optional<Carrier> findAdjascentChest() {
		if (!isValid()) {
			return Optional.empty();
		}
		LocatableBlock block = sign.getLocatableBlock();
		Optional<Direction> direction_opt = block.getBlockState().get(Keys.DIRECTION);
		if (direction_opt.isPresent()) {
			Direction direction = direction_opt.get();
			if (direction.isCardinal()) {
				TileEntityCarrier chest = isChest(block.getLocation().add(direction.getOpposite().asBlockOffset()));
				if (chest != null) {
					return Optional.of(chest);
				}
			}
			TileEntityCarrier chest = isChest(block.getLocation().add(0, 1, 0));
			if (chest != null) {
				return Optional.of(chest);
			}
			chest = isChest(block.getLocation().add(0, -1, 0));
			if (chest != null) {
				return Optional.of(chest);
			}
		}
		return Optional.empty();
	}
	
	private TileEntityCarrier isChest(Location<World> location) {
		Optional<TileEntity> maybe_this_is_a_chest = location.getTileEntity();
		if (maybe_this_is_a_chest.isPresent()) {
			TileEntity is_it_a_chest = maybe_this_is_a_chest.get();
			if (is_it_a_chest instanceof TileEntityCarrier) {
				return (TileEntityCarrier) is_it_a_chest;
			}
		}
		return null;
	}
	
	private String quantityToString(int quantity) {
		return "x" + quantity;
	}
	
	private static String priceToString(double price) {
		double floored = Math.floor(price);
		if (floored != price) {
			DecimalFormat format = new DecimalFormat("#.00");
			return format.format(((double) Math.round(price * 100.0)) / 100.0);
		}
		return ((long) price) + "";
	}
}
