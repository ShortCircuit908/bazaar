package com.shortcircuit.bazaar.event;

import com.flowpowered.math.vector.Vector3d;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.data.type.HandType;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Cancellable;
import org.spongepowered.api.event.Event;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.cause.Cause;
import org.spongepowered.api.event.entity.living.humanoid.HandInteractEvent;
import org.spongepowered.api.event.filter.cause.Root;
import org.spongepowered.api.util.Direction;
import org.spongepowered.api.util.Tristate;

/**
 * @author ShortCircuit908
 * Created on 1/8/2018.
 */
public abstract class DoubleClickBlockEvent<T extends InteractBlockEvent & HandInteractEvent> implements Event, Cancellable {
	private static final HashMap<UUID, ClickData> tracker = new HashMap<>();
	private static final long min_delay = 150;
	private static final long double_click_delay = 350;
	private final T wrapped;
	
	public DoubleClickBlockEvent(T wrapped) {
		this.wrapped = wrapped;
	}
	
	public T getWrappedEvent() {
		return wrapped;
	}
	
	@Override
	public Cause getCause() {
		return wrapped.getCause();
	}
	
	public Direction getTargetSide() {
		return wrapped.getTargetSide();
	}
	
	public HandType getHandType() {
		return wrapped.getHandType();
	}
	
	public Optional<Vector3d> getInteractionPoint() {
		return wrapped.getInteractionPoint();
	}
	
	@Override
	public boolean isCancelled() {
		return wrapped.isCancelled();
	}
	
	@Override
	public void setCancelled(boolean cancel) {
		wrapped.setCancelled(cancel);
	}
	
	public BlockSnapshot getTargetBlock() {
		return wrapped.getTargetBlock();
	}
	
	public static class Primary extends DoubleClickBlockEvent<InteractBlockEvent.Primary.MainHand> {
		public Primary(InteractBlockEvent.Primary.MainHand wrapped) {
			super(wrapped);
		}
	}
	
	public static class Secondary extends DoubleClickBlockEvent<InteractBlockEvent.Secondary.MainHand> {
		
		public Secondary(InteractBlockEvent.Secondary.MainHand wrapped) {
			super(wrapped);
		}
		
		public Tristate getOriginalUseItemResult() {
			return getWrappedEvent().getOriginalUseItemResult();
		}
		
		public Tristate getOriginalUseBlockResult() {
			return getWrappedEvent().getOriginalUseBlockResult();
		}
		
		public Tristate getUseItemResult() {
			return getWrappedEvent().getUseItemResult();
		}
		
		public Tristate getUseBlockResult() {
			return getWrappedEvent().getUseBlockResult();
		}
		
		public void setUseItemResult(Tristate result) {
			getWrappedEvent().setUseItemResult(result);
		}
		
		public void setUseBlockResult(Tristate result) {
			getWrappedEvent().setUseBlockResult(result);
		}
	}
	
	private static class ClickData {
		private ClickData() {
			timestamp = System.currentTimeMillis();
		}
		
		private long timestamp;
		private InteractBlockEvent last_event;
	}
	
	public static class ClickListener {
		@Listener
		public void clickBlock(final InteractBlockEvent event, @Root Player player) {
			if (!(event instanceof InteractBlockEvent.Secondary.MainHand || event instanceof InteractBlockEvent.Primary.MainHand)) {
				return;
			}
			long now = System.currentTimeMillis();
			ClickData data = tracker.computeIfAbsent(player.getUniqueId(), uuid -> new ClickData());
			if (data.last_event != null) {
				if (!event.getTargetBlock().getLocation().equals(data.last_event.getTargetBlock().getLocation())
						|| (data.last_event instanceof InteractBlockEvent.Primary.MainHand && !(event instanceof InteractBlockEvent.Primary.MainHand))
						|| (data.last_event instanceof InteractBlockEvent.Secondary.MainHand && !(event instanceof InteractBlockEvent.Secondary.MainHand))) {
					data.timestamp = 0;
					data.last_event = null;
					return;
				}
				if (data.timestamp + double_click_delay >= now && data.timestamp + double_click_delay - now >= min_delay && data.last_event != null) {
					DoubleClickBlockEvent new_event = event instanceof InteractBlockEvent.Primary.MainHand
							? new DoubleClickBlockEvent.Primary((InteractBlockEvent.Primary.MainHand) event)
							: new DoubleClickBlockEvent.Secondary((InteractBlockEvent.Secondary.MainHand) event);
					Sponge.getEventManager().post(new_event);
					data.timestamp = 0;
					data.last_event = null;
					return;
				}
			}
			data.timestamp = now;
			data.last_event = event;
		}
	}
}
